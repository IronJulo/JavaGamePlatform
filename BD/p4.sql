USE db41a;

CREATE TABLE ROLE (
  nom_role varchar(10),
  PRIMARY KEY (nom_role)
);

CREATE TABLE MESSAGE (
  id_message int AUTO_INCREMENT,
  date_message varchar(100),
  contenu_message varchar(500),
  id_destinataire int,
  id_expediteur int ,
  lu char(1) default 'N',
  id_invitation int default 0,
  PRIMARY KEY (id_message)
);

CREATE TABLE UTILISATEUR (
  id_utilisateur int PRIMARY KEY AUTO_INCREMENT,
  pseudo_utilisateur varchar(10) ,
  email_utilisateur varchar(100),
  mdp_utilisateur varchar(100),
  active_utilisateur char(1) default 'O',
  lien_image_de_profil varchar(500) default 'https://static.thenounproject.com/png/17241-200.png',
  nom_role varchar(10) default 'util'
);

CREATE TABLE GAMES(
  game_name varchar(20),
  lien_image_jeu varchar(500),
  game_description TEXT,
  PRIMARY KEY (game_name)
);

CREATE TABLE ETREAMI (
  id_expediteur int,
  id_destinataire int,
  PRIMARY KEY (id_expediteur, id_destinataire)
);

CREATE TABLE INVITATION (
  id_expediteur int,
  id_destinataire int,
  game_name varchar(20),
  PRIMARY KEY (id_expediteur, id_destinataire)
);


CREATE TABLE PARTIE (
  id_partie int PRIMARY KEY AUTO_INCREMENT,
  debut_partie datetime,
  numero_tape int,
  etat_partie text,
  is_game_end boolean,
  id_expediteur int,
  id_destinataire int,
  score_1 int,
  score_2 int,
  id_gagnant int
);

CREATE TABLE INVITERAMI (
  id_expediteur int,
  id_destinataire int,
  game_name varchar(20),
  PRIMARY KEY (id_expediteur, id_destinataire)
);

CREATE TABLE MATCHMAKING (
  id_wait int
);

ALTER TABLE MESSAGE ADD FOREIGN KEY (id_destinataire) REFERENCES UTILISATEUR (id_utilisateur);
ALTER TABLE UTILISATEUR ADD FOREIGN KEY (nom_role) REFERENCES ROLE (nom_role);
ALTER TABLE ETREAMI ADD FOREIGN KEY (id_expediteur) REFERENCES UTILISATEUR (id_utilisateur);
ALTER TABLE ETREAMI ADD FOREIGN KEY (id_destinataire) REFERENCES UTILISATEUR (id_utilisateur);
ALTER TABLE PARTIE ADD FOREIGN KEY (id_expediteur) REFERENCES UTILISATEUR (id_utilisateur);
ALTER TABLE PARTIE ADD FOREIGN KEY (id_destinataire) REFERENCES UTILISATEUR (id_utilisateur);
ALTER TABLE INVITATION ADD FOREIGN KEY (id_expediteur) REFERENCES UTILISATEUR (id_utilisateur);
ALTER TABLE INVITATION ADD FOREIGN KEY (id_destinataire) REFERENCES UTILISATEUR (id_utilisateur);
ALTER TABLE INVITERAMI ADD FOREIGN KEY (game_name) REFERENCES GAMES(game_name);
ALTER TABLE MATCHMAKING ADD FOREIGN KEY (id_wait) REFERENCES UTILISATEUR(id_utilisateur);
insert into ROLE(nom_role) values ('admin'),('util');
INSERT INTO UTILISATEUR (pseudo_utilisateur, email_utilisateur, mdp_utilisateur, nom_role) VALUES ('admin', 'admin@admin.gg', 'admin', 'admin');
INSERT INTO UTILISATEUR (pseudo_utilisateur, email_utilisateur, mdp_utilisateur, nom_role) VALUES ('test', 'test@test.gg', 'tototo', 'util');
INSERT INTO GAMES (game_name, lien_image_jeu, game_description) VALUES ('PUISSANCE 4', 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQqBQTI3C53JGe2mR7QcY1E2wCIKK0tFaCQwyX5EVjL19Jjzayl&usqp=CAU','Le jeu du puissance 4');
INSERT INTO PARTIE (debut_partie, numero_tape, etat_partie, is_game_end, id_expediteur, id_destinataire, score_1, score_2, id_gagnant) values (CURTIME(),0,'{"1":"1","1":"2","grid":"000000000000000000000000000000000000000000"}', 'false',1,2,0,0,0);
INSERT INTO PARTIE (debut_partie, numero_tape, etat_partie, is_game_end, id_expediteur, id_destinataire, score_1, score_2, id_gagnant) values (CURTIME(),0,'{"last":1,"grid":"000000000000000000000000000000000000000000"}', 'false',1,2,0,0,0);

