package Exceptions.UserCreation;

import Alertes.Alertes;

public class UsernameIncorrectException extends Exception {
    /**
     * Random generated serialVersionUID made by Visual Studio Code
     */
    private static final long serialVersionUID = -3710088980864328072L;

    public UsernameIncorrectException() {
        super("Username incorrect");
    }
    public void showException() {
        Alertes.usernameIncorrect();
    }
}