package Exceptions.UserCreation;

import Alertes.Alertes;

public class PasswordDifferentException extends Exception {
    /**
     * Random generated serialVersionUID made by Visual Studio Code
     */
    private static final long serialVersionUID = -2810644143952048918L;

    public PasswordDifferentException() {
        super("Password different");
    }
    public void showException() {
        Alertes.passwordDifferent();
    }
}
