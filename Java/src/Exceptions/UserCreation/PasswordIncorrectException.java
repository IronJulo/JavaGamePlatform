package Exceptions.UserCreation;

import Alertes.Alertes;

public class PasswordIncorrectException extends Exception {
    /**
     * Random generated serialVersionUID made by Visual Studio Code
     */
    private static final long serialVersionUID = 2670205964605112596L;

    public PasswordIncorrectException() {
        super("Password incorrect");
    }
    public void showException() {
        Alertes.passwordIncorrect();
    }
}

