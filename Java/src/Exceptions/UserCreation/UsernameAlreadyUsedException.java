package Exceptions.UserCreation;

import Alertes.Alertes;

public class UsernameAlreadyUsedException extends Exception {
    /**
     * Random generated serialVersionUID made by Visual Studio Code
     */
    private static final long serialVersionUID = -1785881275791282916L;

    public UsernameAlreadyUsedException() {
        super("Username already used");
    }
    public void showException() {
        Alertes.usenameAlreadyUsed();
    }
}
