package Exceptions.UserCreation;

import Alertes.Alertes;

public class LoginIncorrectException extends Exception {
    /**
     * Random generated serialVersionUID made by Visual Studio Code
     */
    private static final long serialVersionUID = 2593624909915348059L;

    public LoginIncorrectException() {
        super("Mail incorrect");
    }
    public void showException() {
        Alertes.loginIncorrect();
    }
}