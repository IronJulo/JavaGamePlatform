package Exceptions.UserCreation;

import Alertes.Alertes;

public class MailIncorrectException extends Exception {
    /**
     * Random generated serialVersionUID made by Visual Studio Code
     */
    private static final long serialVersionUID = 4890599655280926535L;

    public MailIncorrectException() {
        super("Mail incorrect");
    }
    public void showException() {
        Alertes.mailIncorrect();
    }
}
