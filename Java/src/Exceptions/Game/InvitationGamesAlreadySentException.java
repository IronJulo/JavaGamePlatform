package Exceptions.Game;

import Alertes.Alertes;

public class InvitationGamesAlreadySentException extends Exception {
    /**
     * Random generated serialVersionUID made by Visual Studio Code
     */
    private static final long serialVersionUID = -2525014919953985136L;

    public InvitationGamesAlreadySentException() {
        super("Invitation Games Already Sent");
    }

    public void showException() {
        Alertes.invitationGamesAlreadySent();
    }
}