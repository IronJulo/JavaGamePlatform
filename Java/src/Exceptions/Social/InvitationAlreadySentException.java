package Exceptions.Social;

import Alertes.Alertes;

public class InvitationAlreadySentException extends Exception {
    /**
     * Random generated serialVersionUID made by Visual Studio Code
     */
    private static final long serialVersionUID = 3815228802451021019L;

    public InvitationAlreadySentException() {
        super("Invitation Already Sent");
    }
    public void showException() {
        Alertes.invitationAlreadySent();
    }
}