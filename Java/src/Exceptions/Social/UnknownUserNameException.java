package Exceptions.Social;

import Alertes.Alertes;

public class UnknownUserNameException extends Exception {
    /**
     * Random generated serialVersionUID made by Visual Studio Code
     */
    private static final long serialVersionUID = 1660840260128367642L;

    public UnknownUserNameException() {
        super("Unknown User");
    }
    public void showException() {
        Alertes.unknownUser();
    }
}