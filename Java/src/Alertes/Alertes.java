package Alertes;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;

import java.util.Optional;

public class Alertes {
    /**
     * Alert user that the username is alerady used
     */
    public static void usenameAlreadyUsed(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Username already use");
        alert.setHeaderText(null);
        alert.setContentText("The username you choose is already used please choose an other one");
        alert.showAndWait();
    }

    /**
     * Alert user that the username is incorrect
     */
    public static void usernameIncorrect(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Incorrect username");
        alert.setHeaderText(null);
        alert.setContentText("Please use at least:\n - 3 characters");
        alert.showAndWait();
    }

    /**
     * Alert user that the password is incorrect
     */
    public static void passwordIncorrect(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Incorrect password");
        alert.setHeaderText(null);
        alert.setContentText("Please use at least:\n - 5 characters");
        alert.showAndWait();
    }

    /**
     * Alert user that the mail is incorrect
     */
    public static void mailIncorrect(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Incorrect Mail Adress");
        alert.setHeaderText(null);
        alert.setContentText("Please type a valid mail adress");
        alert.showAndWait();
    }

    /**
     * Alert user that the login is incorrect
     */
    public static void loginIncorrect(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Incorrect Login");
        alert.setHeaderText(null);
        alert.setContentText("Please type a valid login");
        alert.showAndWait();
    }

    /**
     * Alert user that passwords typed are different
     */
    public static void passwordDifferent(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Different Password");
        alert.setHeaderText(null);
        alert.setContentText("Please type a new password and confirm it");
        alert.showAndWait();
    }

    /**
     * Alert user that the picture link is incorrect
     */
    public static void pPIncorrectLink() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Incorrect link");
        alert.setHeaderText(null);
        alert.setContentText("Please type a correct link to an image (png, gif, ...)");
        alert.showAndWait();
    }

    /**
     * Alert user that the user is unknown
     */
    public static void unknownUser() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Unknown User");
        alert.setHeaderText(null);
        alert.setContentText("There is no player with this name");
        alert.showAndWait();
    }

    /**
     * Alert user that the invitation is already sent
     */
    public static void invitationAlreadySent() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invitation already sent");
        alert.setHeaderText(null);
        alert.setContentText("You have already sent an invitation to this person wait for her to accept");
        alert.showAndWait();
    }

    /**
     * Alert user that the image Link invalid
     */
    public static void invalidImageLink() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Image Link");
        alert.setHeaderText(null);
        alert.setContentText("this Image Link is Invalid");
        alert.showAndWait();
    }

    /**
     * Alert user that the invitation is sent
     */
    public static void invitationSent() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Invitation sent");
        alert.setHeaderText(null);
        alert.setContentText("You have sent an invitation to this person wait for her to accept");
        alert.showAndWait();
    }

    /**
     * Alert user that the game invitation is sent
     */
    public static void invitationGameSent() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game invitation sent");
        alert.setHeaderText(null);
        alert.setContentText("You have sent a game invitation to this person wait for her to accept");
        alert.showAndWait();
    }

    /**
     * Alert user that the user have to wait
     */
    public static void userHaveToWait() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Wait for an opponent");
        alert.setHeaderText(null);
        alert.setContentText("You have been put in queue");
        alert.showAndWait();
    }

    /**
     * Alert user that he added a friend
     */
    public static void friendAdded() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("You are now friend");
        alert.setHeaderText(null);
        alert.setContentText("You are now friend you can talk and play together!");
        alert.showAndWait();
    }

    /**
     * Alert user that he is not an administrator
     */
    public static void notAnAdministrator() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("You are not an administrator");
        alert.setHeaderText(null);
        alert.setContentText("You are not an administrator !\n Please contact one!");
        alert.showAndWait();
    }

    /**
     * Alert user that he already send this invitation
     */
	public static void invitationGamesAlreadySent() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invitation Games already sent");
        alert.setHeaderText(null);
        alert.setContentText("You have already sent a Game invitation to this person wait for her to accept");
        alert.showAndWait();
	}
    /**
     * Alert user and ask confirmation to revoke his admin rights
     */
	public static boolean confirmRights(String title, String header, String content){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        ImageView icon = new ImageView("https://thumbs.gfycat.com/SphericalIdealHairstreakbutterfly-size_restricted.gif");
        icon.setPreserveRatio(true);
        icon.setFitHeight(80);
        alert.getDialogPane().setGraphic(icon);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    public static boolean confirmDeleteAccount() {
	    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        ImageView icon = new ImageView("https://thumbs.gfycat.com/SphericalIdealHairstreakbutterfly-size_restricted.gif");
        icon.setPreserveRatio(true);
        icon.setFitHeight(80);
        alert.getDialogPane().setGraphic(icon);
        alert.setTitle("Delete account");
        alert.setHeaderText("Are you sure about that ?");
        alert.setContentText(null);
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;

    }
}
