package Games;

public class Games {
    private String gameName, gamePictureLink, gameDescription;

    /**
     *
     * @param gameName
     * @param gamePictureLink
     * @param gameDescription
     */
    public Games(String gameName, String gamePictureLink, String gameDescription) {
        this.gameName = gameName;
        this.gamePictureLink = gamePictureLink;
        this.gameDescription = gameDescription;
    }

    /**
     * returns the game name
     * @return the game name
     */
    public String getGameName() {
        return gameName;
    }

    /**
     * returns the game picture link
     * @return the game picture link
     */
    public String getGamePictureLink() {
        return gamePictureLink;
    }

    /**
     * returns the game desctiption
     * @return the game desctiption
     */
    public String getGameDescription() {
        return gameDescription;
    }

    @Override
    public String toString() {
        return "Games{" +
                "gameName='" + gameName + '\'' +
                ", gamePictureLink='" + gamePictureLink + '\'' +
                ", gameDescription='" + gameDescription + '\'' +
                '}';
    }
}
