package Users;

public class User {
    private int id;
    private boolean active;
    private String name;
    private String mail;
    private String password;
    private String ppLink;
    private String nomrole;

    /**
     *
     * @param id
     * @param name
     * @param mail
     * @param password
     * @param active
     * @param ppLink
     * @param nomrole
     */
    public User (String id, String name, String mail, String password, String active, String ppLink, String nomrole){
        this.id = Integer.parseInt(id);
        this.active = active.equals("O");
        this.name = name;
        this.mail = mail;
        this.password = password;
        this.ppLink = ppLink;
        this.nomrole = nomrole;
    }

    /**
     *
     * @return the user id
     */
    public int getId() {
        return id;
    }

    /**
     *method that set the user id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * returns true if the user is actived
     * @return true if the user is actived
     */
    public boolean isActive() {
        return active;
    }

    /**
     * method that set if the user is activated
     * @param active
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * returns the user name
     * @return the user name
     */
    public String getName() {
        return name;
    }

    /**
     * method that set the user username
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * returns the user mail
     * @return the user mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * method that set the user mail
     * @param mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * returns the user password
     * @return the user password
     */
    public String getPassword() {
        return password;
    }

    /**
     * method that set the user password
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * returns the user picture link
     * @return the user picture link
     */
    public String getPpLink() {
        return ppLink;
    }

    /**
     * method that set the user pplink
     * @param ppLink
     */
    public void setPpLink(String ppLink) {
        this.ppLink = ppLink;
    }

    /**
     * returns the user role
     * @return the user role
     */
    public String getNomrole() {
        return nomrole;
    }

    /**
     * method that set the user role
     * @param nomrole
     */
    public void setNomrole(String nomrole) {
        this.nomrole = nomrole;
    }
}
