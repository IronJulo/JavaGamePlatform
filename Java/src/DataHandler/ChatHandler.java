package DataHandler;

import Gui.Applications.App;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ChatHandler {
    private JdbcConnector jdbcConnector;
    private App application;

    /**
     *
     * @param jdbcConnector
     * @param application
     */
    public ChatHandler(JdbcConnector jdbcConnector, App application) {
        this.jdbcConnector = jdbcConnector;
        this.application = application;
    }

    /**
     * returns the list of formated messages
     * @param idFriend
     * @return the list of formated messages
     */
    public List<HBox> getMessages(String idFriend){
        ArrayList<HBox> res = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.jdbcConnector.getConnection().prepareStatement("SELECT * from MESSAGE where id_destinataire = ? and id_expediteur = ? or id_destinataire = ? and id_expediteur = ?");
            preparedStatement.setString(1, String.valueOf(this.application.getCurrentUser().getId()));
            preparedStatement.setString(2, idFriend);
            preparedStatement.setString(3, idFriend);
            preparedStatement.setString(4, String.valueOf(this.application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                if (resultSet.getString(7).equals("0")){
                    res.add(formatedResultset(resultSet.getString(2), resultSet.getString(3), resultSet.getString(5)));
                    if (resultSet.getString(6).equals("N")){
                        PreparedStatement preparedStatement2 = this.jdbcConnector.getConnection().prepareStatement("update MESSAGE set lu = ?  where id_destinataire = ? and id_expediteur = ? ");
                        preparedStatement2.setString(1, "O");
                        preparedStatement2.setString(2, String.valueOf(this.application.getCurrentUser().getId()));
                        preparedStatement2.setString(3, idFriend);
                        preparedStatement2.executeQuery();
                    }
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the formated message
     * @param dateMessage
     * @param textMessage
     * @param idExpediteur
     * @return the formated message
     */
    public HBox formatedResultset(String dateMessage, String textMessage, String idExpediteur) {
        HBox res = new HBox();
        VBox vBox = new VBox();
        Label date = new Label(dateMessage);
        date.setFont(new Font(10));
        Label message = new Label(textMessage);
        message.setFont(new Font(20));
        message.setWrapText(true);
        vBox.getChildren().addAll(date, message);
        res.getChildren().add(vBox);
        if (idExpediteur.equals(String.valueOf(this.application.getCurrentUser().getId()))){
            vBox.setAlignment(Pos.CENTER_RIGHT);
            res.setAlignment(Pos.CENTER_RIGHT);
        }else{
            vBox.setAlignment(Pos.CENTER_LEFT);
            res.setAlignment(Pos.CENTER_LEFT);
        }
        return res;
    }

    /**
     * Method that send the message to the friend
     * @param messgeData
     * @param idDestinataire
     */
    public void sendMessage(String messgeData, String idDestinataire){
        if (! messgeData.equals("")){
            try {
                PreparedStatement preparedStatement = this.jdbcConnector.getConnection().prepareStatement("INSERT INTO MESSAGE (date_message, contenu_message, id_destinataire, id_expediteur ) VALUES (?, ?, ?, ?)");
                preparedStatement.setString(1, DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format(LocalDateTime.now()));
                preparedStatement.setString(2, messgeData);
                preparedStatement.setString(3, idDestinataire);
                preparedStatement.setString(4, String.valueOf(this.application.getCurrentUser().getId()));
                preparedStatement.executeQuery();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}