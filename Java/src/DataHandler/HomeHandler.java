package DataHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Gui.Applications.App;
import Gui.Controlers.ControllerJoinGiveUpParty;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;


public class HomeHandler {
    private JdbcConnector jdbcConnector;
    private App application;

    /**
     *
     * @param jdbcConnector
     * @param application
     */
    public HomeHandler(JdbcConnector jdbcConnector, App application) {
        this.jdbcConnector = jdbcConnector;
        this.application = application;
    }

    /**
     *
     * @return the list of the recent party
     */
	public List<HBox> getRecentParty() {
        ArrayList<HBox> res = new ArrayList<>();
		try {
            PreparedStatement preparedStatement1 = this.jdbcConnector.getConnection().prepareStatement("select * from PARTIE where id_expediteur = ? and is_game_end = true or id_destinataire = ? and is_game_end = true");
            preparedStatement1.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement1.setString(2, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()){
                res.add(formatRecentPartyResult(resultSet.getInt(6), resultSet.getInt(7), resultSet.getInt(10)));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
	}

    /**
     *
     * @return the list of all pending party
     */
	public List<HBox> getPendingParty() {
        ArrayList<HBox> res = new ArrayList<>();
		try {
            PreparedStatement preparedStatement1 = this.jdbcConnector.getConnection().prepareStatement("select * from PARTIE where id_expediteur = ? and is_game_end = false or id_destinataire = ? and is_game_end = false");
            preparedStatement1.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement1.setString(2, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()){
                res.add(formatPendingPartyResult(resultSet.getInt(1), resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(2)));
            }         
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the formated recent party
     * @param id_destinataire
     * @param id_expediteur
     * @param id_gagnant
     * @return the formated recent party
     */
    public HBox formatRecentPartyResult(int id_destinataire, int id_expediteur, int id_gagnant){
        HBox hbox = new HBox(10);
        VBox vBox = new VBox(10), vBox2 = new VBox(10);
        String joueur1 = DataChecker.getUsername(this.jdbcConnector, this.application, id_destinataire),
        joueur2 = DataChecker.getUsername(this.jdbcConnector, this.application, id_expediteur),
        joueurGagnant = DataChecker.getUsername(this.jdbcConnector, this.application, id_gagnant);
        Label lGamename = new Label("Game : Connect 4"),
        lPlayers = new Label("Players : " + joueur1 + " vs " + joueur2),
        lgagne = new Label(joueurGagnant + " won the game");
        lGamename.setFont(new Font(20));
        lPlayers.setFont(new Font(20));
        lgagne.setFont(new Font(20));
        vBox.getChildren().addAll(lGamename, lPlayers);
        vBox2.getChildren().add(lgagne);
        hbox.getChildren().addAll(vBox, vBox2);
        hbox.setPadding(new Insets(10,10,10,10));
        hbox.setStyle("-fx-background-color: #7e7e7e; -fx-background-radius: 20;");
        return hbox;
    }

    /**
     * returns the formated pending party
     * @param idPartie
     * @param id_destinataire
     * @param id_expediteur
     * @param dateDebut
     * @return the formated pending party
     */
    public HBox formatPendingPartyResult(int idPartie, int id_destinataire, int id_expediteur, String dateDebut){
        String joueur1 = DataChecker.getUsername(this.jdbcConnector, this.application, id_destinataire), 
        joueur2 = DataChecker.getUsername(this.jdbcConnector, this.application, id_expediteur);
        HBox hBox = new HBox(10), h1 = new HBox(), h2 = new HBox();
        Button br = new Button("Join the game"), bg = new Button("Give up");
        ControllerJoinGiveUpParty c1 = new ControllerJoinGiveUpParty(this.application, idPartie, id_expediteur, id_destinataire);
        br.setOnAction(c1);
        bg.setOnAction(c1);
        Label j1 = new Label(joueur1), vs = new Label("vs"), j2 = new Label(joueur2), id = new Label(Integer.toString(idPartie));
        h1.getChildren().addAll(j1, vs, j2);
        h2.getChildren().addAll(br, bg, id);
        hBox.getChildren().addAll(h1,h2);
        return hBox;
    }
}