package DataHandler;

import Exceptions.UserCreation.*;
import Gui.Applications.App;

import java.sql.*;

public class SettingsHandler {
    private JdbcConnector Jdbcconnector;
    private Connection connection;
    private App application;

    /**
     *
     * @param Jdbcconnector
     * @param application
     */
    public SettingsHandler(JdbcConnector Jdbcconnector, App application) {
        this.Jdbcconnector = Jdbcconnector;
        this.connection = this.Jdbcconnector.getConnection();
        this.application = application;

    }

    /**
     * Method that change the user username
     * @param userName
     */
    public void changeUsername(String userName) {
        try {
            if ( DataChecker.isUsernameCorrect(userName) && !DataChecker.isUsernameAlreadyUsed(userName, this.Jdbcconnector)) {
                PreparedStatement preparedStatement = this.connection.prepareStatement("update UTILISATEUR set pseudo_utilisateur = ? where pseudo_utilisateur = ?");
                preparedStatement.setString(1, userName);
                preparedStatement.setString(2, this.application.getCurrentUser().getName());
                preparedStatement.executeQuery();
                this.application.getCurrentUser().setName(userName);
            }
        } catch (UsernameIncorrectException e) {
            e.showException();
        } catch (UsernameAlreadyUsedException e) {
            e.showException();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Method that change the user mail
     * @param mail
     */
    public void changeMail(String mail) {
        try {
            if (DataChecker.isMailCorrect(mail)){
                PreparedStatement preparedStatement = this.connection.prepareStatement("update UTILISATEUR set  email_utilisateur = ? where pseudo_utilisateur = ?");
                preparedStatement.setString(1, mail);
                preparedStatement.setString(2, this.application.getCurrentUser().getName());
                preparedStatement.executeQuery();
                this.application.getCurrentUser().setMail(mail);
            }
        } catch (MailIncorrectException e) {
            e.showException();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Method that change the user password
     * @param userPassword
     * @param userConfirmedPassword
     */
    public void changePassword(String userPassword, String userConfirmedPassword) {
        try {
            if (DataChecker.arePasswordEquals(userPassword, userConfirmedPassword) && DataChecker.isUserPasswordCorrect(userPassword)){
                PreparedStatement preparedStatement = this.connection.prepareStatement("update UTILISATEUR set  mdp_utilisateur = ? where pseudo_utilisateur = ?");
                preparedStatement.setString(1, userPassword);
                preparedStatement.setString(2, this.application.getCurrentUser().getName());
                preparedStatement.executeQuery();
                this.application.getCurrentUser().setPassword(userPassword);
            }
        } catch (PasswordIncorrectException e) {
            e.showException();
        } catch (PasswordDifferentException e) {
            e.showException();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Method that change the user profile picture
     * @param profilPicture
     */
    public void changeProfilPicture(String profilPicture) {
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement("update UTILISATEUR set  lien_image_de_profil = ? where pseudo_utilisateur = ?");
            preparedStatement.setString(1, profilPicture);
            preparedStatement.setString(2, this.application.getCurrentUser().getName());
            preparedStatement.executeQuery();
            this.application.getCurrentUser().setPpLink(profilPicture);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void deleteAccount() {

        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement("DELETE FROM UTILISATEUR where pseudo_utilisateur = ?");
            preparedStatement.setString(1, this.application.getCurrentUser().getName());
            preparedStatement.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
