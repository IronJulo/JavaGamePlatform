package DataHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Alertes.Alertes;
import Exceptions.Game.InvitationGamesAlreadySentException;
import Games.Games;
import Gui.Applications.App;
import Gui.Controlers.Game.ControllerAcceptGame;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class GamesHandler {

    /**
     * Method that send the invitation
     * @param jdbcConnector
     * @param application
     * @param friendName
     * @param gameName
     */
    public static void sendInvitationGame(JdbcConnector jdbcConnector, App application, String friendName, String gameName) {
        try {
            if (!DataChecker.isInvitationGamesAleradySend(application, DataChecker.getIdOf(friendName, jdbcConnector), jdbcConnector, gameName)) {
                PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("Insert into INVITATION (id_expediteur, id_destinataire, game_name) values (?, ?, ?)");
                preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
                preparedStatement.setString(2, DataChecker.getIdOf(friendName, jdbcConnector));
                preparedStatement.setString(3, gameName);
                preparedStatement.executeQuery();
                Alertes.invitationGameSent();
            }
        } catch (InvitationGamesAlreadySentException e) {
            e.showException();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * returns the list of all the platform games
     * @param jdbcConnector
     * @param application
     * @return the list of all the platform games
     */
    public static List<Games> getGameList(JdbcConnector jdbcConnector, App application){
        ArrayList<Games> res = new ArrayList<>();
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select * from GAMES").executeQuery();
            while (resultSet.next()) {
                res.add(new Games(resultSet.getString("game_name"), resultSet.getString("lien_image_jeu"), resultSet.getString("game_description")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the list of all game invitations
     * @param jdbcConnector
     * @param application
     * @return the list of all game invitations
     */
    public List<HBox> getGameInvitationList(JdbcConnector jdbcConnector, App application){
        ArrayList<HBox> res = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select * from INVITATION where id_destinataire = ?");
            preparedStatement.setInt(1, application.getCurrentUser().getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                res.add(formatGameInvitation(resultSet.getInt(1), resultSet.getString(3), application));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the formated game invitation
     * @param id_expediteur
     * @param gameName
     * @param application
     * @return the formated game invitation
     */
    public HBox formatGameInvitation(int id_expediteur, String gameName, App application){
        HBox hBox = new HBox(10);
        VBox vBox1 = new VBox(), vBox2 = new VBox();
        vBox2.setAlignment(Pos.CENTER);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(5,30,10,30));
        vBox2.setSpacing(10);
        HBox.setHgrow(vBox1, Priority.ALWAYS);
        hBox.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #a4a4a4;");
        Label name = new Label(DataChecker.getUsername(application.getConnector(), application, id_expediteur)), invite = new Label("Invited you to play \n" + gameName);
        name.setFont(new Font(30));
        Button button1 = new Button("Accept Game Invitation"), button2 = new Button("Delete Game Invitation");
        ControllerAcceptGame g = new ControllerAcceptGame(application, id_expediteur, gameName);
        button1.setOnAction(g);
        button2.setOnAction(g);
        button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #00FF19; " + "-fx-font-size : 15;");
        button1.setOnMouseEntered(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 15;" + "-fx-text-fill:white;"));
        button1.setOnMouseExited(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #00FF19; " + "-fx-font-size : 15;"));
        button1.setOnMousePressed(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 15;" + "-fx-text-fill:white;" + "-fx-scale-x: 1.1;" + "-fx-scale-y: 1.1;"));
        button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #ff0000; " + "-fx-font-size : 15;");
        button2.setOnMouseEntered(e -> button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 15;" + "-fx-text-fill:white;"));
        button2.setOnMouseExited(e -> button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #ff0000; " + "-fx-font-size : 15;"));
        button2.setOnMousePressed(e -> button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 15;" + "-fx-text-fill:white;" + "-fx-scale-x: 1.1;" + "-fx-scale-y: 1.1;"));
        vBox1.getChildren().addAll(name, invite);
        vBox2.getChildren().addAll(button1, button2);
        hBox.getChildren().addAll(vBox1, vBox2);
        return hBox;
    }
}