package DataHandler;

import Gui.Applications.App;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectFourlHandler {
    private JdbcConnector jdbcConnector;

    /**
     *
     * @param jdbcConnector
     * @param application
     */
    public ConnectFourlHandler(JdbcConnector jdbcConnector, App application) {
        this.jdbcConnector = jdbcConnector;
    }

    /**
     * returns the json file in a string
     * @param idPartie
     * @return the json file in a string
     */
    public String getJsonFile(Integer idPartie){
        String res = "";
        try {
            PreparedStatement preparedStatement = this.jdbcConnector.getConnection().prepareStatement("SELECT etat_partie from PARTIE where id_partie = ?");
            preparedStatement.setInt(1, idPartie);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                res = resultSet.getString(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the id of the party owner
     * @param idPartie
     * @return the id of the party owner
     */
    public String getPartieOwnerId(Integer idPartie){
        String res = "";
        try {
            PreparedStatement preparedStatement = this.jdbcConnector.getConnection().prepareStatement("SELECT id_expediteur from PARTIE where id_partie = ?");
            preparedStatement.setInt(1, idPartie);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                res = resultSet.getString(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the id of the party guest
     * @param idPartie
     * @return the id of the party guest
     */
    public String getPartieGuessId(Integer idPartie){
        String res = "";
        try {
            PreparedStatement preparedStatement = this.jdbcConnector.getConnection().prepareStatement("SELECT id_destinataire from PARTIE where id_partie = ?");
            preparedStatement.setInt(1, idPartie);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                res = resultSet.getString(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * Method that write to the db the end of a party
     * @param idPartie
     * @param idGagnant
     * @param jdbcConnector
     */
    public static void partyOver(Integer idPartie, Integer idGagnant, JdbcConnector jdbcConnector){
        try {
            PreparedStatement preparedStatement2 = jdbcConnector.getConnection().prepareStatement("update PARTIE set is_game_end = ? where id_partie = ? ");
            preparedStatement2.setInt(1, 1);

            preparedStatement2.setInt(2, idPartie);
            preparedStatement2.executeQuery();

            PreparedStatement preparedStatement3 = jdbcConnector.getConnection().prepareStatement("update PARTIE set id_gagnant = ? where id_partie = ? ");

            preparedStatement3.setInt(1, idGagnant);
            preparedStatement3.setInt(2, idPartie);
            preparedStatement3.executeQuery();

        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     *Method that write to the db the end of a party
     * @param idPartie
     * @param idGagnant
     */
    public void partyOver(Integer idPartie, Integer idGagnant){
        try {
            PreparedStatement preparedStatement2 = this.jdbcConnector.getConnection().prepareStatement("update PARTIE set is_game_end = ? where id_partie = ? ");
            preparedStatement2.setInt(1, 1);

            preparedStatement2.setInt(2, idPartie);
            preparedStatement2.executeQuery();

            PreparedStatement preparedStatement3 = this.jdbcConnector.getConnection().prepareStatement("update PARTIE set id_gagnant = ? where id_partie = ? ");

            preparedStatement3.setInt(1, idGagnant);
            preparedStatement3.setInt(2, idPartie);
            preparedStatement3.executeQuery();

        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Method that rewrite the json file
     * @param idPartie
     * @param jsonContent
     */
    public void reWriteJsonFile(Integer idPartie, String jsonContent){
        try {
            PreparedStatement preparedStatement2 = this.jdbcConnector.getConnection().prepareStatement("update PARTIE set etat_partie = ?  where id_partie = ? ");
            preparedStatement2.setString(1, jsonContent);
            preparedStatement2.setInt(2, idPartie);
            preparedStatement2.executeQuery();
        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
