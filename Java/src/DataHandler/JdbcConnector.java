package DataHandler;

import java.sql.Connection;
import java.sql.DriverManager;

public class JdbcConnector {
    private String userName, password, url, driver;
    private Connection connection;

    /**
     * The connector
     */
    public JdbcConnector() {
        userName = "groupe41a";
        password = "20@info!iuto41a";
        url = "jdbc:mariadb://46.105.92.223/db41a";
        driver = "org.mariadb.jdbc.Driver";

        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, userName, password);
            System.out.println("Connection is successful");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

}