package DataHandler;

import Exceptions.UserCreation.LoginIncorrectException;
import Gui.Applications.App;

public class LoginHandler {
    private JdbcConnector Jdbcconnector;

    /**
     *
     * @param Jdbcconnector
     * @param application
     */
    public LoginHandler(JdbcConnector Jdbcconnector, App application) {
        this.Jdbcconnector = Jdbcconnector;
    }

    /**
     * returns true if the login is correct
     * @param userName
     * @param userPassword
     * @return true if the login is correct
     */
    public boolean checkLogin(String userName, String userPassword) {
        try {
            return DataChecker.checkLogin(userName, userPassword, this.Jdbcconnector);
        } catch (LoginIncorrectException e) {
            e.showException();
        }
        return false;
    }

    /**
     * Method that set the user
     * @param application
     * @param userName
     * @param jdbcConnector
     */
    public void setUser(App application, String userName, JdbcConnector jdbcConnector) {
        DataChecker.setUser(userName, application, jdbcConnector);
    }
}
