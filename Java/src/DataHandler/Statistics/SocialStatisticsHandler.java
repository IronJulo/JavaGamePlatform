package DataHandler.Statistics;

import DataHandler.JdbcConnector;
import Gui.Applications.App;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SocialStatisticsHandler {

    /**
     *the number of sent invitations the number of sent invitations
     * @param jdbcConnector
     * @param application
     * @return the number of sent invitations
     */
    public static String getNSentInvitation(JdbcConnector jdbcConnector, App application){
    try {
        PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select count(*) from INVITERAMI where id_expediteur = ? ");
        preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getString(1);
    } catch (SQLException throwables) {
        throwables.printStackTrace();
    }
    return "0";
    }

    /**
     * returns the number of received invitations
     * @param jdbcConnector
     * @param application
     * @return the number of received invitations
     */
    public static String getNReceivedInvitation(JdbcConnector jdbcConnector, App application){
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select count(*) from INVITERAMI where id_destinataire = ? ");
            preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return "0";
    }

    /**
     * Returns the number of unread messages
     * @param jdbcConnector
     * @param application
     * @return the number of unread messages
     */
    public static String getNUnreadMessages(JdbcConnector jdbcConnector, App application){
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select count(*) from MESSAGE where id_destinataire = ? and lu = ?");
            preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement.setString(2, "N");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return "0";
    }

    /**
     * returns the number of friends
     * @param jdbcConnector
     * @param application
     * @return the number of friends
     */
    public static String getNFriends(JdbcConnector jdbcConnector, App application){
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select count(*) from ETREAMI where id_expediteur = ? or id_destinataire = ?");
            preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement.setString(2, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return String.valueOf(Integer.parseInt(resultSet.getString(1))/2);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return "0";
    }
}
