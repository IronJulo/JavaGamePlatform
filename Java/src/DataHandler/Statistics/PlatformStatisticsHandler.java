package DataHandler.Statistics;

import DataHandler.JdbcConnector;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PlatformStatisticsHandler {
    /**
     * Returns the list of formated platform statistics
     * @param jdbcConnector
     * @return the list of formated platform statistics
     */
    public static List<HBox> getStatistics(JdbcConnector jdbcConnector) {
        ArrayList<HBox> statistics = new ArrayList<>();
        statistics.add(formatedData("Number of players", getNumberOfPlayers(jdbcConnector)));
        statistics.add(formatedData("Number of admins players", getNumberOfAdminPlayers(jdbcConnector)));
        statistics.add(formatedData("Number of non admin players", getNumberOfNonAdminPlayers(jdbcConnector)));
        statistics.add(formatedData("Number of disabled players", getNumberOfActivatedPlayers(jdbcConnector)));
        statistics.add(formatedData("Number of activated admin players", getNumberOfActivatedAdminPlayers(jdbcConnector)));
        statistics.add(formatedData("Number of activated non admin players", getNumberOfActivatedNonAdminPlayers(jdbcConnector)));
        statistics.add(formatedData("Number of disabled players", getNumberOfdisabledPlayers(jdbcConnector)));
        statistics.add(formatedData("Number of disabled admin players", getNumberOfdisabledAdminPlayers(jdbcConnector)));
        statistics.add(formatedData("Number of disabled non admin players", getNumberOfdisabledNonAdminPlayers(jdbcConnector)));
        statistics.add(formatedData("Number of party", getNumberOfParty(jdbcConnector)));
        return statistics;
    }

    /**
     * Returns the HBox containing the data
     * @param dataName
     * @param data
     * @return the HBox containing the data
     */
    public static HBox formatedData(String dataName, String data){
        HBox hbox = new HBox();
        HBox hbox1 = new HBox();
        HBox hbox2 = new HBox();
        Label lData = new Label(data);
        Label lDataName = new Label(dataName);
        hbox1.getChildren().addAll(lDataName);
        hbox2.getChildren().add(lData);
        hbox1.setAlignment(Pos.CENTER_LEFT);
        hbox2.setAlignment(Pos.CENTER_RIGHT);
        lDataName.setFont(new Font(30));
        lData.setFont(new Font(30));
        HBox.setHgrow(hbox1, Priority.ALWAYS);
        HBox.setHgrow(hbox2, Priority.ALWAYS);
        hbox.getChildren().addAll(hbox1, hbox2);
        lData.setTextAlignment(TextAlignment.RIGHT);

        return hbox;
    }

    /**
     * Returns the number of players null if SQLExcepetion
     * @param jdbcConnector
     * @return the number of players null if SQLExcepetion
     */
    public static String getNumberOfPlayers(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from UTILISATEUR").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the number of admin players null if SQLException
     * @param jdbcConnector
     * @return the number of admin players null if SQLException
     */
    public static String getNumberOfAdminPlayers(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from UTILISATEUR where nom_role = 'admin'").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the number of non admin players null if SQLException
     * @param jdbcConnector
     * @return the number of non admin players null if SQLException
     */
    public static String getNumberOfNonAdminPlayers(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from UTILISATEUR where nom_role = 'util'").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the number of activated players null if SQLException
     * @param jdbcConnector
     * @return the number of activated players null if SQLException
     */
    public static String getNumberOfActivatedPlayers(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from UTILISATEUR where active_utilisateur = 'O'").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the number of activated admin players null if SQLException
     * @param jdbcConnector
     * @return the number of activated admin players null if SQLException
     */
    public static String getNumberOfActivatedAdminPlayers(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from UTILISATEUR where nom_role = 'admin' and active_utilisateur = 'O'").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the number of activated non admin players null if SQLException
     * @param jdbcConnector
     * @return the number of activated non admin players null if SQLException
     */
    public static String getNumberOfActivatedNonAdminPlayers(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from UTILISATEUR where nom_role = 'util' and active_utilisateur = 'O'").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the number of disabled players null if SQLException
     * @param jdbcConnector
     * @return the number of disabled players null if SQLException
     */
    public static String getNumberOfdisabledPlayers(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from UTILISATEUR where active_utilisateur = 'N'").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the number of disabled admin players null if SQLException
     * @param jdbcConnector
     * @return the number of disabled admin players null if SQLException
     */
    public static String getNumberOfdisabledAdminPlayers(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from UTILISATEUR where nom_role = 'admin' and active_utilisateur = 'N'").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the number of disabled non admin players null if SQLException
     * @param jdbcConnector
     * @return the number of disabled non admin players null if SQLException
     */
    public static String getNumberOfdisabledNonAdminPlayers(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from UTILISATEUR where nom_role = 'util' and active_utilisateur = 'N'").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the number of party null if SQLException
     * @param jdbcConnector
     * @return the number of party null if SQLException
     */
    public static String getNumberOfParty(JdbcConnector jdbcConnector){
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select count(*) from PARTIE").executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
