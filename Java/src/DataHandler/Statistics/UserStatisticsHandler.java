package DataHandler.Statistics;

import DataHandler.JdbcConnector;
import Gui.Applications.App;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserStatisticsHandler {

    /**
     * returns the list of formated statistics
     * @param jdbcConnector
     * @param application
     * @return the list of formated statistics
     */
    public static List<HBox> getStatistics(JdbcConnector jdbcConnector, App application) {
        ArrayList<HBox> statistics = new ArrayList<>();
        statistics.add(formatedData("Number of friends ", getNumberOfFriends(jdbcConnector, application)));
        statistics.add(formatedData("Number of party ", getNumberOfParty(jdbcConnector, application)));
        statistics.add(formatedData("Number of invitations ", getNumberOfGameInvitation(jdbcConnector, application)));
        return statistics;
    }

    /**
     * returns the HBox of the formated data
     * @param dataName
     * @param data
     * @return the HBox of the formated data
     */
    public static HBox formatedData(String dataName, String data){
        HBox hbox = new HBox();
        HBox hbox1 = new HBox();
        HBox hbox2 = new HBox();
        Label lData = new Label(data);
        Label lDataName = new Label(dataName);
        hbox1.getChildren().add(lDataName);
        hbox2.getChildren().add(lData);
        hbox1.setAlignment(Pos.CENTER_LEFT);
        hbox2.setAlignment(Pos.CENTER_RIGHT);
        lDataName.setFont(new Font(30));
        lData.setFont(new Font(30));
        HBox.setHgrow(hbox1,Priority.ALWAYS);
        HBox.setHgrow(hbox2,Priority.ALWAYS);
        hbox.getChildren().addAll(hbox1,hbox2);
        lData.setTextAlignment(TextAlignment.RIGHT);

        return hbox;
    }

    /**
     * returns the number of friends
     * @param jdbcConnector
     * @param application
     * @return the number of friends
     */
    public static String getNumberOfFriends(JdbcConnector jdbcConnector, App application){
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select distinct count(*) from ETREAMI where id_expediteur = ?");
            preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * returns the number of invitation
     * @param jdbcConnector
     * @param application
     * @return the number of invitation
     */
    public static String getNumberOfGameInvitation(JdbcConnector jdbcConnector, App application){
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select distinct count(*) from INVITATION where id_destinataire = ?");
            preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * returns the number of party
     * @param jdbcConnector
     * @param application
     * @return the number of party
     */
    public static String getNumberOfParty(JdbcConnector jdbcConnector, App application){
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select distinct count(*) from PARTIE where id_destinataire = ?");
            preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * returns the ResultSet of the user
     * @param userId
     * @param jdbcConnector
     * @return the ResultSet of the user
     */
    public static ResultSet getDataFrom(String userId, JdbcConnector jdbcConnector){
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select * from UTILISATEUR where id_utilisateur = ? ");
            preparedStatement.setString(1, userId);
            return preparedStatement.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
