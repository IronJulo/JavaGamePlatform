package DataHandler;

import Gui.Applications.App;
import Gui.Controlers.ControllerUserCells;
import Users.User;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ManagePlayerHandler {
    private JdbcConnector jdbcConnector;
    private App application;

    /**
     *
     * @param jdbcConnector
     * @param application
     */
    public ManagePlayerHandler(JdbcConnector jdbcConnector, App application) {
        this.jdbcConnector = jdbcConnector;
        this.application = application;
    }

    /**
     * returns the list of all users
     * @return the list of all users
     */
    public List<User> getUsers(){
        ArrayList<User> res = new ArrayList<>();
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select * from UTILISATEUR").executeQuery();
            while (resultSet.next()){
                res.add(new User(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7)));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the formated user list
     * @param userList
     * @return the formated user list
     */
    public List<HBox> getFormatedUserList(List<User> userList) {
        ArrayList<HBox> res = new ArrayList<>();
        for (User user : userList){
            res.add(formatUser(user));
        }
        return res;
    }

    /**
     * returns the formated user
     * @param user
     * @return the formated user
     */
    private HBox formatUser(User user){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Ressources/Scenes/UserCell.fxml"));
        try {
            Parent scene = (Parent) loader.load();
            ControllerUserCells controller = loader.getController();
            controller.setUser(user);
            controller.setApplication(this.application);
            return (HBox) scene;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
