package DataHandler;

import Alertes.Alertes;
import DataHandler.Statistics.UserStatisticsHandler;
import Exceptions.Social.InvitationAlreadySentException;
import Exceptions.Social.UnknownUserNameException;
import Gui.Applications.App;
import Gui.Controlers.ControllerFriendCell;
import Gui.Controlers.ControllerFriendRequestSend;
import Gui.Controlers.ControllerReceivedFriendRequest;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SocialHandler {
    private JdbcConnector jdbcConnector;
    private App application;

    /**
     *
     * @param jdbcConnector
     * @param application
     */
    public SocialHandler(JdbcConnector jdbcConnector, App application) {
        this.jdbcConnector = jdbcConnector;
        this.application = application;
    }

    /**
     * Method that insert the invitation in the db
     * @param jdbcConnector
     * @param application
     * @param friendName
     */
    public void sendInvitation(JdbcConnector jdbcConnector, App application, String friendName) {
        if (!friendName.equals(application.getCurrentUser().getName())){
            try {
                if (DataChecker.isUsernameUsed(friendName, jdbcConnector) && ! DataChecker.isInvitationAleradySend(application, DataChecker.getIdOf(friendName, jdbcConnector), jdbcConnector)){
                    String friendId = DataChecker.getIdOf(friendName, jdbcConnector);
                    PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("Insert into INVITERAMI (id_expediteur, id_destinataire) values (?, ?)");
                    preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
                    preparedStatement.setString(2, friendId);
                    preparedStatement.executeQuery();
                    Alertes.invitationSent();
                    if (DataChecker.isInvitationMutual(jdbcConnector, application, friendId)){
                        Alertes.friendAdded();
                        setFriend(jdbcConnector, application, friendId);
                    }
                }

            } catch (InvitationAlreadySentException e) {
                e.showException();
            } catch (UnknownUserNameException e) {
                e.showException();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    /**
     * Method that set friends
     * @param jdbcConnector
     * @param application
     * @param friendId
     */
    public void setFriend(JdbcConnector jdbcConnector, App application, String friendId){
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("DELETE FROM INVITERAMI WHERE id_expediteur = ? and id_destinataire = ?");
            preparedStatement1.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement1.setString(2, friendId);
            preparedStatement1.executeQuery();
            preparedStatement1.setString(1, friendId);
            preparedStatement1.setString(2, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement1.executeQuery();
            PreparedStatement preparedStatement2 = jdbcConnector.getConnection().prepareStatement("INSERT INTO ETREAMI (id_expediteur, id_destinataire) VALUES (?, ?)");
            preparedStatement2.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement2.setString(2, friendId);
            preparedStatement2.executeQuery();
            preparedStatement2.setString(1, friendId);
            preparedStatement2.setString(2, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement2.executeQuery();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Method that delete friends
     * @param jdbcConnector
     * @param application
     * @param friendId
     */
    public void deleteFriend(JdbcConnector jdbcConnector, App application, String friendId){
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("DELETE FROM ETREAMI WHERE id_expediteur = ? and id_destinataire = ? or id_expediteur = ? and id_destinataire = ?");
            preparedStatement1.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement1.setString(2, friendId);
            preparedStatement1.setString(3, friendId);
            preparedStatement1.setString(4, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement1.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Method that accept friends
     * @param jdbcConnector
     * @param application
     * @param friendId
     */
    public void acceptFriendRequest(JdbcConnector jdbcConnector, App application, String friendId){
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("DELETE FROM INVITERAMI WHERE id_expediteur = ? and id_destinataire = ?");
            preparedStatement1.setString(1, friendId);
            preparedStatement1.setString(2, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement1.executeQuery();
            PreparedStatement preparedStatement2 = jdbcConnector.getConnection().prepareStatement("INSERT INTO ETREAMI (id_expediteur, id_destinataire) VALUES (?, ?)");
            preparedStatement2.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement2.setString(2, friendId);
            preparedStatement2.executeQuery();
            preparedStatement2.setString(1, friendId);
            preparedStatement2.setString(2, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement2.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Method that delet friend request
     * @param jdbcConnector
     * @param application
     * @param friendId
     */
    public void deleteFriendRequest(JdbcConnector jdbcConnector, App application, String friendId){
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("DELETE FROM INVITERAMI WHERE id_expediteur = ? and id_destinataire = ?");
            preparedStatement1.setString(1, friendId);
            preparedStatement1.setString(2, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement1.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Method that cancel friend request
     * @param jdbcConnector
     * @param application
     * @param friendId
     */
    public void cancelFriendRequest(JdbcConnector jdbcConnector, App application, String friendId){
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("DELETE FROM INVITERAMI WHERE id_expediteur = ? and id_destinataire = ?");
            preparedStatement1.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement1.setString(2, friendId);
            preparedStatement1.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * returns the friend list of the user
     * @return the friend list of the user
     */
    public List<HBox> getFriendList() {
        ArrayList<HBox> res = new ArrayList<>();
        try {
            PreparedStatement preparedStatement1 = this.jdbcConnector.getConnection().prepareStatement("select * from ETREAMI where id_expediteur = ?");
            preparedStatement1.setString(1, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()){
                ResultSet friendData = UserStatisticsHandler.getDataFrom(resultSet.getString(2), this.jdbcConnector);
                friendData.next();
                res.add(formatResultset(friendData.getString(1), friendData.getString(2), friendData.getString(3), friendData.getString(6), friendData.getString(7)));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the friend request list of the user
     * @return the friend request list of the user
     */
    public List<HBox> getFriendRequestList() {
        ArrayList<HBox> res = new ArrayList<>();
        try {
            PreparedStatement preparedStatement1 = this.jdbcConnector.getConnection().prepareStatement("select * from INVITERAMI where id_expediteur = ?");
            preparedStatement1.setString(1, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()){
                ResultSet friendData = UserStatisticsHandler.getDataFrom(resultSet.getString(2), this.jdbcConnector);
                friendData.next();
                res.add(formatSendResultset(friendData.getString(1), friendData.getString(2), friendData.getString(3), friendData.getString(6), friendData.getString(7)));
            }                        
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the received friend request list of the user
     * @return the received friend request list of the user
     */
    public List<HBox> getFriendReceivedRequestList() {
        ArrayList<HBox> res = new ArrayList<>();
        try {
            PreparedStatement preparedStatement1 = this.jdbcConnector.getConnection().prepareStatement("select * from INVITERAMI where id_destinataire = ?");
            preparedStatement1.setString(1, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()){
                ResultSet friendData = UserStatisticsHandler.getDataFrom(resultSet.getString(1), this.jdbcConnector);
                friendData.next();
                res.add(formatReceivedResultset(friendData.getString(1), friendData.getString(2), friendData.getString(3), friendData.getString(6), friendData.getString(7)));
            }                        
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the formated resultset
     * @param idFriend
     * @param pseudoFriend
     * @param mailFriend
     * @param pPLink
     * @param roleName
     * @return the formated resultset
     */
    public HBox formatResultset(String idFriend, String pseudoFriend, String mailFriend, String pPLink, String roleName){
        HBox hBox = new HBox(10);
        VBox vBox1 = new VBox(), vBox2 = new VBox();
        vBox2.setAlignment(Pos.CENTER);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(10,50,10,50));
        vBox2.setSpacing(10);
        HBox.setHgrow(vBox1, Priority.ALWAYS);
        if (roleName.equals("admin")){
            hBox.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #c38a00;");
        }else{
            hBox.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #a4a4a4;");
        }
        ImageView imageView = new ImageView(pPLink);
        imageView.setPreserveRatio(true);
        imageView.setFitHeight(100);

        Label name = new Label(pseudoFriend), mail = new Label(mailFriend);
        name.setFont(new Font(30));
        mail.setFont(new Font(20));
        Button button1 = new Button("Chat"), button2 = new Button("Delete Friend");
        ControllerFriendCell controllerFriendCell = new ControllerFriendCell();
        controllerFriendCell.setApplication(this.application);
        controllerFriendCell.setFriend(idFriend, this);
        button1.setOnAction(controllerFriendCell);
        button2.setOnAction(controllerFriendCell);
        button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #00FF19; " + "-fx-font-size : 20;");
        button1.setOnMouseEntered(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;"));
        button1.setOnMouseExited(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #00FF19; " + "-fx-font-size : 20;"));
        button1.setOnMousePressed(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;" + "-fx-scale-x: 1.1;" + "-fx-scale-y: 1.1;"));
        button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #ff0000; " + "-fx-font-size : 20;");
        button2.setOnMouseEntered(e -> button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;"));
        button2.setOnMouseExited(e -> button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #ff0000; " + "-fx-font-size : 20;"));
        button2.setOnMousePressed(e -> button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;" + "-fx-scale-x: 1.1;" + "-fx-scale-y: 1.1;"));
        vBox1.getChildren().addAll(name, mail);
        vBox2.getChildren().addAll(button1, button2);
        hBox.getChildren().addAll(imageView, vBox1, vBox2);

        return hBox;
    }

    /**
     * returns the formated result set
     * @param idFriend
     * @param pseudoFriend
     * @param mailFriend
     * @param pPLink
     * @param roleName
     * @return the formated result set
     */
    public HBox formatSendResultset(String idFriend, String pseudoFriend, String mailFriend, String pPLink, String roleName){
        HBox hBox = new HBox(10);
        VBox vBox1 = new VBox(), vBox2 = new VBox();
        vBox2.setAlignment(Pos.CENTER);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(10,50,10,50));
        vBox2.setSpacing(10);
        HBox.setHgrow(vBox1, Priority.ALWAYS);
        if (roleName.equals("admin")){
            hBox.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #c38a00;");
        }else{
            hBox.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #a4a4a4;");
        }
        ImageView imageView = new ImageView(pPLink);
        imageView.setPreserveRatio(true);
        imageView.setFitHeight(100);

        Label name = new Label(pseudoFriend), mail = new Label(mailFriend);
        name.setFont(new Font(30));
        mail.setFont(new Font(20));
        Button button1 = new Button("Cancel Friend request");
        button1.setOnAction(new ControllerFriendRequestSend(this.application, this, idFriend));
        button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #00FF19; " + "-fx-font-size : 20;");
        button1.setOnMouseEntered(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;"));
        button1.setOnMouseExited(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #00FF19; " + "-fx-font-size : 20;"));
        button1.setOnMousePressed(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;" + "-fx-scale-x: 1.1;" + "-fx-scale-y: 1.1;"));
        vBox1.getChildren().addAll(name, mail);
        vBox2.getChildren().add(button1);
        hBox.getChildren().addAll(imageView, vBox1, vBox2);
        return hBox;
    }

    /**
     * returns the formated result set
     * @param idFriend
     * @param pseudoFriend
     * @param mailFriend
     * @param pPLink
     * @param roleName
     * @return the formated result set
     */
    public HBox formatReceivedResultset(String idFriend, String pseudoFriend, String mailFriend, String pPLink, String roleName){
        HBox hBox = new HBox(10);
        VBox vBox1 = new VBox(), vBox2 = new VBox();
        vBox2.setAlignment(Pos.CENTER);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(10,50,10,50));
        vBox2.setSpacing(10);
        HBox.setHgrow(vBox1, Priority.ALWAYS);
        if (roleName.equals("admin")){
            hBox.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #c38a00;");
        }else{
            hBox.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #a4a4a4;");
        }
        ImageView imageView = new ImageView(pPLink);
        imageView.setPreserveRatio(true);
        imageView.setFitHeight(100);

        Label name = new Label(pseudoFriend), mail = new Label(mailFriend);
        name.setFont(new Font(30));
        mail.setFont(new Font(20));
        Button button1 = new Button("Accept Friend Request"), button2 = new Button("Delete Friend Request");
        ControllerReceivedFriendRequest c1 = new ControllerReceivedFriendRequest(this.application, this, idFriend);
        button1.setOnAction(c1);
        button2.setOnAction(c1);
        button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #00FF19; " + "-fx-font-size : 20;");
        button1.setOnMouseEntered(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;"));
        button1.setOnMouseExited(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #00FF19; " + "-fx-font-size : 20;"));
        button1.setOnMousePressed(e -> button1.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;" + "-fx-scale-x: 1.1;" + "-fx-scale-y: 1.1;"));
        button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #ff0000; " + "-fx-font-size : 20;");
        button2.setOnMouseEntered(e -> button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;"));
        button2.setOnMouseExited(e -> button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: #ff0000; " + "-fx-font-size : 20;"));
        button2.setOnMousePressed(e -> button2.setStyle("-fx-background-radius: 100; " + "-fx-background-color: rgb(39, 39, 39); " + "-fx-font-size : 20;" + "-fx-text-fill:white;" + "-fx-scale-x: 1.1;" + "-fx-scale-y: 1.1;"));
        vBox1.getChildren().addAll(name, mail);
        vBox2.getChildren().addAll(button1, button2);
        hBox.getChildren().addAll(imageView, vBox1, vBox2);

        return hBox;
    }
}
