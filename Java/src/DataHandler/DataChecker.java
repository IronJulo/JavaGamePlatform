package DataHandler;

import DataHandler.Statistics.UserStatisticsHandler;
import Exceptions.Game.InvitationGamesAlreadySentException;
import Exceptions.Social.InvitationAlreadySentException;
import Exceptions.Social.UnknownUserNameException;
import Exceptions.UserCreation.*;
import Gui.Applications.App;
import Users.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DataChecker {
    /**
     * Return true if username is not already used
     * @param userName user's name
     * @param jdbcConnector DB connector
     * @return true if username is correct
     * @throws UsernameAlreadyUsedException
     */
    public static boolean isUsernameAlreadyUsed(String userName, JdbcConnector jdbcConnector) throws UsernameAlreadyUsedException {
        boolean res = false;
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select pseudo_utilisateur from UTILISATEUR").executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString(1).equals(userName)) {
                    res = true;
                    throw new UsernameAlreadyUsedException();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * Return true if username is correct
     * @param userName user's name
     */
    public static boolean isUsernameCorrect(String userName) throws UsernameIncorrectException {
        if (userName.length() > 2) return true;
        throw new UsernameIncorrectException();        
    }
    
    /**
     * Return true if user's mail adress is correct
     * @param mail user's mail adress
     */
    public static boolean isMailCorrect(String mail) throws MailIncorrectException {
        if (mail.contains("@")) return true;
        throw new MailIncorrectException();
    }

    /**
     * Return true if user's password is correct
     * @param userPassword user's password
     */
    public static boolean isUserPasswordCorrect(String userPassword) throws PasswordIncorrectException {
        if (userPassword.length() > 4) return true;
        throw new PasswordIncorrectException();
    }

    /**
     * Return true if userPassword and userConfirmedPassword are the same
     * @param userPassword
     * @param userConfirmedPassword
     * @return true if passwords are the same
     * @throws PasswordDifferentException
     */
    public static boolean arePasswordEquals(String userPassword, String userConfirmedPassword)throws PasswordDifferentException {
        if (userPassword.equals(userConfirmedPassword)) return true;
        throw new PasswordDifferentException();
    }

    /**
     * returns true if the username and password match
     * @param userName
     * @param userPassword
     * @param jdbcConnector
     * @return true if the username and password match
     * @throws LoginIncorrectException
     */
    public static boolean checkLogin(String userName, String userPassword, JdbcConnector jdbcConnector) throws LoginIncorrectException {
        try {
            ResultSet resultSet =jdbcConnector.getConnection().prepareStatement("select pseudo_utilisateur, mdp_utilisateur from UTILISATEUR").executeQuery();
            while (resultSet.next()) if (resultSet.getString(1).equals(userName) && resultSet.getString(2).equals(userPassword)) return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        throw new LoginIncorrectException();
    }

    /**
     * Method that set the user
     * @param userName
     * @param application
     * @param jdbcConnector
     */
    public static void setUser(String userName, App application, JdbcConnector jdbcConnector) {
        try (PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select * from UTILISATEUR where pseudo_utilisateur = ? ")) {
            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            application.setCurrentUser(new User(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7)));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * returns true if the form is correct
     * @param userName
     * @param userPassword
     * @param mail
     * @param jdbcConnector
     * @return true if the form is correct
     */
    public static boolean isFormCorrect(String userName, String userPassword, String mail, JdbcConnector jdbcConnector) {
        try {
            return !isUsernameAlreadyUsed(userName, jdbcConnector) && isUsernameCorrect(userName) && isUserPasswordCorrect(userPassword) && isMailCorrect(mail);
        } catch (UsernameAlreadyUsedException e) {
            e.showException();
        } catch (PasswordIncorrectException e) {
            e.showException();
        } catch (MailIncorrectException e) {
            e.showException();
        } catch (UsernameIncorrectException e) {
            e.showException();
        }
        return false;
    }

    /**
     * returns true if the user have been created
     * @param userName
     * @param userPassword
     * @param mail
     * @param jdbcConnector
     * @return true if the user have been created
     */
    public static boolean createUser(String userName, String userPassword, String mail, JdbcConnector jdbcConnector) {
        boolean res = false;
        if (isFormCorrect(userName, userPassword, mail, jdbcConnector)) {
            try (PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("INSERT INTO UTILISATEUR (pseudo_utilisateur, email_utilisateur, mdp_utilisateur) VALUES (?, ?, ?)")) {
                preparedStatement.setString(1, userName);
                preparedStatement.setString(2, mail);
                preparedStatement.setString(3, userPassword);
                preparedStatement.execute();
                res = true;
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        return res;
    }

    /**
     * returns the id that correspond to the username
     * @param friendName
     * @param jdbcConnector
     * @return the id that correspond to the username
     */
    public static String getIdOf(String friendName, JdbcConnector jdbcConnector) {
        try (PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select id_utilisateur from UTILISATEUR where pseudo_utilisateur = ? ")) {
            preparedStatement.setString(1, friendName);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    /**
     * returns true if the username is used
     * @param userName
     * @param jdbcConnector
     * @return true if the username is used
     * @throws UnknownUserNameException
     */
    public static boolean isUsernameUsed(String userName, JdbcConnector jdbcConnector) throws UnknownUserNameException {
        try {
            ResultSet resultSet = jdbcConnector.getConnection().prepareStatement("select pseudo_utilisateur from UTILISATEUR").executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString(1).equals(userName)) {
                    return true;
                }
            }
            throw new UnknownUserNameException();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * returns true if the invitation is already sent
     * @param application
     * @param friendId
     * @param jdbcConnector
     * @return true if the invitation is already sent
     * @throws InvitationAlreadySentException
     */
    public static boolean isInvitationAleradySend(App application, String friendId, JdbcConnector jdbcConnector) throws InvitationAlreadySentException {
        boolean res = false;
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select id_destinataire from INVITERAMI where id_expediteur = ?");
            preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString(1).equals(friendId)) {
                    res = true;
                    throw new InvitationAlreadySentException();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns true if the game invitation is already sent
     * @param application
     * @param friendId
     * @param jdbcConnector
     * @param gameName
     * @return true if the game invitation is already sent
     * @throws InvitationGamesAlreadySentException
     */
    public static boolean isInvitationGamesAleradySend(App application, String friendId, JdbcConnector jdbcConnector, String gameName) throws InvitationGamesAlreadySentException {
        boolean res = false;
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select id_destinataire from INVITATION where id_expediteur = ? and game_name = ?"); 
            preparedStatement.setString(1, String.valueOf(application.getCurrentUser().getId()));
            preparedStatement.setString(2, gameName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString(1).equals(friendId)) {
                    res = true;
                    throw new InvitationGamesAlreadySentException();
                }
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns true if the friend request are mutual
     * @param jdbcConnector
     * @param application
     * @param friendId
     * @return true if the friend request are mutual
     */
    public static boolean isInvitationMutual(JdbcConnector jdbcConnector, App application, String friendId) {
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select id_destinataire from INVITERAMI where id_expediteur = ? ");
            preparedStatement.setString(1, friendId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) if (resultSet.getString(1).equals(String.valueOf(application.getCurrentUser().getId()))) return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * returns the friend list of the user
     * @param jdbcConnector
     * @param application
     * @return the friend list of the user
     */
    public static List<String> getFriendList(JdbcConnector jdbcConnector, App application){
        ArrayList<String> res = new ArrayList<>();
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("select * from ETREAMI where id_expediteur = ?");
            preparedStatement1.setString(1, String.valueOf(application.getCurrentUser().getId()));
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()){
                ResultSet friendData = UserStatisticsHandler.getDataFrom(resultSet.getString(2), jdbcConnector);
                friendData.next();
                res.add(friendData.getString("pseudo_utilisateur"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the image link of the userId
     * @param jdbcConnector
     * @param application
     * @param id
     * @return the image link of the userId
     */
    public static String getImageLink(JdbcConnector jdbcConnector, App application, int id){
        String res = "";
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("select lien_image_de_profil from UTILISATEUR where id_utilisateur = ?");
            preparedStatement1.setInt(1, id);
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()){
                res = resultSet.getString(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * returns the username of the userId
     * @param jdbcConnector
     * @param application
     * @param id
     * @return the username of the userId
     */
    public static String getUsername(JdbcConnector jdbcConnector, App application, int id){
        String res = "";
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("select pseudo_utilisateur from UTILISATEUR where id_utilisateur = ?");
            preparedStatement1.setInt(1, id);
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()){
                res = resultSet.getString(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * Method that create a party in the db
     * @param jdbcConnector
     * @param application
     * @param id_expediteur
     */
	public static void createParty(JdbcConnector jdbcConnector, App application, int id_expediteur) {
        try{
        Calendar cal = Calendar.getInstance(); 
        java.sql.Timestamp timestamp = new Timestamp(cal.getTimeInMillis());
        PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("INSERT INTO PARTIE (debut_partie, numero_tape, etat_partie, is_game_end, id_expediteur, id_destinataire, score_1, score_2, id_gagnant) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        preparedStatement1.setTimestamp(1, timestamp);
        preparedStatement1.setInt(2, 0);
        preparedStatement1.setString(3, "{\"last\":" + id_expediteur + ",\"grid\":\"000000000000000000000000000000000000000000\"}");
        preparedStatement1.setBoolean(4, false);
        preparedStatement1.setInt(5, id_expediteur);
        preparedStatement1.setInt(6, application.getCurrentUser().getId());
        preparedStatement1.setInt(7, 0);
        preparedStatement1.setInt(8, 0);
        preparedStatement1.setInt(9, 0);
        preparedStatement1.execute();
        } catch (SQLException throwables){
            throwables.printStackTrace();
        }
    }

    /**
     * returns the if of the last party played
     * @param jdbcConnector
     * @param application
     * @return the if of the last party played
     */
    public static Integer getLastParty(JdbcConnector jdbcConnector, App application){
        Integer res = 1;
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("select Max(id_partie) from PARTIE");
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()){
                res = resultSet.getInt(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    /**
     * Method that delete the invitation
     * @param jdbcConnector
     * @param application
     * @param id_expediteur
     * @param gameName
     */
	public static void deleteInvitationParty(JdbcConnector jdbcConnector, App application, int id_expediteur, String gameName) {
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("DELETE FROM INVITATION where id_expediteur = ? and game_name = ? and id_destinataire = ?");
            preparedStatement1.setInt(1, id_expediteur);
            preparedStatement1.setString(2, gameName);
            preparedStatement1.setInt(3, application.getCurrentUser().getId());
            preparedStatement1.executeQuery();  
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
	}

    /**
     * returns true if someone is waiting in queu
     * @param jdbcConnector
     * @param application
     * @return true if someone is waiting in queu
     */
	public static boolean someoneWait(JdbcConnector jdbcConnector, App application) {
        boolean res = false;
		try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("SELECT COUNT(*) from MATCHMAKING");
            ResultSet resultSet = preparedStatement1.executeQuery();
            resultSet.next();
            if(resultSet.getInt(1)>0){
                res = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
	}

    /**
     * returns the id of the user waiting
     * @param jdbcConnector
     * @param application
     * @return the id of the user waiting
     */
	public static int getWaiterId(JdbcConnector jdbcConnector, App application) {
        int res = 0;
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("SELECT * from MATCHMAKING");
            ResultSet resultSet = preparedStatement1.executeQuery();
            resultSet.next();
            res = resultSet.getInt(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
		return res;
	}

    /**
     * Method that delete the waiter from the matchmaking table
     * @param jdbcConnector
     * @param application
     * @param idWait
     */
	public static void deleteWaiter(JdbcConnector jdbcConnector, App application, int idWait) {
        try {
            PreparedStatement preparedStatement1 = jdbcConnector.getConnection().prepareStatement("DELETE FROM MATCHMAKING where id_wait = ?");
            preparedStatement1.setInt(1, idWait);
            preparedStatement1.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
	}

    /**
     * Method that put tue user un the queu
     * @param jdbcConnector
     * @param application
     */
	public static void waitUser(JdbcConnector jdbcConnector, App application) {
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("INSERT INTO MATCHMAKING (id_wait) VALUES (?)");
            preparedStatement.setInt(1, application.getCurrentUser().getId());
            preparedStatement.executeQuery();
        } catch (SQLException throwables) {
            
        }
	}

    /**
     * returnsa the list of all platform user
     * @param jdbcConnector
     * @param application
     * @return the list of all platform user
     */
    public static List<String> getAllUser(JdbcConnector jdbcConnector, App application) {
        ArrayList<String> res = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("select pseudo_utilisateur from UTILISATEUR");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                res.add(resultSet.getString(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

}
