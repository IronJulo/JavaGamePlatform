package DataHandler;

import Gui.Applications.App;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AdministrationHandler {
    private JdbcConnector jdbcConnector;
    private App application;

    /**
     *
     * @param jdbcConnector
     * @param application
     */
    public AdministrationHandler(JdbcConnector jdbcConnector, App application) {
        this.jdbcConnector = jdbcConnector;
        this.application = application;
    }

    /**
     * Method that set the user role
     * @param jdbcConnector
     * @param name
     * @param role
     */
    public static void setUserRole(JdbcConnector jdbcConnector, String name, String role) {
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("update UTILISATEUR set nom_role = ? where pseudo_utilisateur = ?");
            preparedStatement.setString(1, role);
            preparedStatement.setString(2, name);
            preparedStatement.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     *  Method that revoke administrator rights
     */
    public void revokeRight(){
        try {
            PreparedStatement preparedStatement = this.jdbcConnector.getConnection().prepareStatement("update UTILISATEUR set nom_role = ? where pseudo_utilisateur = ?");
            preparedStatement.setString(1, "util");
            preparedStatement.setString(2, this.application.getCurrentUser().getName());
            preparedStatement.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    /**
     * Method that set the user state
     * @param jdbcConnector
     * @param username
     * @param state
     */
    public static void setUserState(JdbcConnector jdbcConnector, String username, String state){
        try {
            PreparedStatement preparedStatement = jdbcConnector.getConnection().prepareStatement("update UTILISATEUR set active_utilisateur = ? where pseudo_utilisateur = ?");
            preparedStatement.setString(1, state);
            preparedStatement.setString(2, username);
            preparedStatement.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
