package DataHandler;

import Gui.Applications.App;

public class RegisterHandler {
    private JdbcConnector Jdbcconnector;
    private App application;

    /**
     *
     * @param Jdbcconnector
     * @param application
     */
    public RegisterHandler(JdbcConnector Jdbcconnector, App application) {
        this.Jdbcconnector = Jdbcconnector;
        this.application = application;
    }

    /**
     * Method that insert a new user in the db
     * @param userName
     * @param userPassword
     * @param userMail
     */
    public void createUser(String userName, String userPassword, String userMail) {
        if (DataChecker.createUser(userName, userPassword, userMail, this.Jdbcconnector)){
            this.application.setUrlSceneFXML("Login.fxml");
            this.application.maj();
        }
    }
}
