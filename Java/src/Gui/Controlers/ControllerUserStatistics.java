package Gui.Controlers;

import Alertes.Alertes;
import DataHandler.Statistics.UserStatisticsHandler;
import Gui.Applications.App;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;

public class ControllerUserStatistics implements Controller {

    public Button BHome;
    public Button BGames;
    public Button BSocial;
    public Button BAdministration;
    public Button BStatistics;
    public ListView<HBox> LVStatistics;

    private App application;

    /**
     * method that set the app
     * @param application
     */
    public void setApplication(App application) {
        this.application = application;
        this.LVStatistics.setItems(FXCollections.observableArrayList(UserStatisticsHandler.getStatistics(this.application.getConnector(), this.application)));
    }

    /**
     *  method that  change the scene to home
     * @param actionEvent
     */
    public void goHome(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method that  change the scene to games
     * @param actionEvent
     */
    public void goGames(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Games.fxml");
        this.application.maj();
    }

    /**
     * method that  change the scene to social
     * @param actionEvent
     */
    public void goSocial(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Social.fxml");
        this.application.maj();
    }

    /**
     * method that  change the scene to userstatistics
     * @param actionEvent
     */
    public void goStatistics(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("UserStatistics.fxml");
        this.application.maj();
    }

    /**
     * method that  change the scene to administration
     * @param actionEvent
     */
    public void goAdministration(ActionEvent actionEvent) {
        if (this.application.getCurrentUser().getNomrole().equals("admin")) {
            this.application.setUrlSceneFXML("Administration.fxml");
            this.application.maj();
        }
        else Alertes.notAnAdministrator();
    }
}