package Gui.Controlers;

import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

public class CloseChat implements EventHandler<WindowEvent> {

    @Override
    public void handle(WindowEvent event) {
        ControllerFriendCell.timeline.stop();
    }

}
