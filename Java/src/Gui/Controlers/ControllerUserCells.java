package Gui.Controlers;

import DataHandler.AdministrationHandler;
import Gui.Applications.App;
import Users.User;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ControllerUserCells implements Controller {
    public ImageView IVUser;
    public Label LUsername;
    public Label LMail;
    public Button BActivateDesactivate;
    public Button BChangeAdminState;

    private App application;
    private User user;

    /**
     * method that handle the buttons
     * @param actionEvent
     */
    public void changeUserState(ActionEvent actionEvent) {
        if (((Button) actionEvent.getTarget()).getText().equals("Desactivate")){
            AdministrationHandler.setUserState(this.application.getConnector(),this.user.getName(), "N");
            this.application.maj();
        }else{
            AdministrationHandler.setUserState(this.application.getConnector(),this.user.getName(), "O");
            this.application.maj();
        }
    }

    /**
     * method that change the user role
     * @param actionEvent
     */
    public void changeAdminState(ActionEvent actionEvent) {
        if (((Button) actionEvent.getTarget()).getText().equals("Promote Administrator")){
            AdministrationHandler.setUserRole(this.application.getConnector(),this.user.getName(), "admin");
            this.application.maj();
        }else{
            AdministrationHandler.setUserRole(this.application.getConnector(),this.user.getName(), "util");
            this.application.maj();
        }
    }

    /**
     * method that set the app
     * @param app
     */
    @Override
    public void setApplication(App app) {
        this.application = app;
        this.IVUser.setImage(new Image(this.user.getPpLink()));
        this.LUsername.setText(this.user.getName());
        this.LMail.setText(this.user.getMail());
        if (this.user.isActive()){
            this.BActivateDesactivate.setText("Desactivate");
            this.BActivateDesactivate.setStyle("-fx-background-color: #ff0000; ");
        }else{
            this.BActivateDesactivate.setText("Activate");
            this.BActivateDesactivate.setStyle("-fx-background-color: #26ff00; ");
        }
        if (this.user.getNomrole().equals("util")){
            this.BChangeAdminState.setText("Promote Administrator");
            this.BChangeAdminState.setStyle("-fx-background-color: #26ff00; ");
        }else{
            this.BChangeAdminState.setText("Revoke rights");
            this.BChangeAdminState.setStyle("-fx-background-color: #ff0000; ");
        }
    }

    /**
     * method that set the user
     * @param user
     */
    public void setUser(User user){
        this.user = user;
    }
}
