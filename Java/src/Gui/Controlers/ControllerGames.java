package Gui.Controlers;

import Alertes.Alertes;
import DataHandler.GamesHandler;
import Games.Games;
import Gui.Applications.App;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ControllerGames implements Controller {
    public Button BHome;
    public Button BGames;
    public Button BSocial;
    public Button BAdministration;
    public Button BStatistics;
    public ListView<Parent> LVGames;
    public ListView<HBox> LVInvite;

    private App application;
    private List<Games> gamesList;
    private List<Parent> games;
    private GamesHandler gm;
    private List<HBox> itemList;

    /**
     * method that set the app
     * @param application
     */
    public void setApplication(App application) {
        this.application = application;
        this.games = new ArrayList<>();
        this.gamesList = GamesHandler.getGameList(this.application.getConnector(), this.application);
        for (Games game : this.gamesList) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Ressources/Scenes/GamesCellSmall.fxml"));
            try {
                Parent scene = (Parent) loader.load();
                ControllerGamesCells controller = loader.getController();
                controller.setGame(game);
                controller.setApplication(this.application);
                games.add(scene);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.LVGames.setItems(FXCollections.observableArrayList(this.games));
        this.gm = new GamesHandler();
        this.itemList = this.gm.getGameInvitationList(this.application.getConnector(), this.application);
        this.LVInvite.setItems(FXCollections.observableArrayList(this.itemList));
    }

    /**
     * method that change the scene to home
     * @param actionEvent
     */

    public void goHome(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to games
     * @param actionEvent
     */
    public void goGames(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Games.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to social
     * @param actionEvent
     */
    public void goSocial(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Social.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to userstatistics
     * @param actionEvent
     */
    public void goStatistics(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("UserStatistics.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to administration
     * @param actionEvent
     */
    public void goAdministration(ActionEvent actionEvent) {
        if (this.application.getCurrentUser().getNomrole().equals("admin")) {
            this.application.setUrlSceneFXML("Administration.fxml");
            this.application.maj();
        }
        else Alertes.notAnAdministrator();
    }
}
