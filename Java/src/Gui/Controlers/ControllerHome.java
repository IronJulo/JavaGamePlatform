package Gui.Controlers;

import java.util.List;

import Alertes.Alertes;
import DataHandler.HomeHandler;
import Gui.Applications.App;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;


public class ControllerHome implements Controller {

    public Button BHome;
    public Button BGames;
    public Button BSocial;
    public Button BAdministration;
    public Label LUsername;
    public Label LMail;
    public Button BLogOut;
    public ListView<HBox> LVRecentParty;
    public ListView<HBox> LVPendingParty;
    public ImageView IVProfile;
    public Button BStatistics;
    private App application;
    private HomeHandler hHandler;
    private List<HBox> itemListRecentParty;
    private List<HBox> itemListPendingParty;

    /**
     *
     * @param application
     */
    public void setApplication(App application) {
        this.application = application;
        this.LUsername.setText(this.application.getCurrentUser().getName());
        this.LMail.setText(this.application.getCurrentUser().getMail());
        this.IVProfile.setImage(new Image(this.application.getCurrentUser().getPpLink()));
        this.hHandler = new HomeHandler(this.application.getConnector(), this.application);
        this.itemListRecentParty = this.hHandler.getRecentParty();
        this.LVRecentParty.setItems(FXCollections.observableArrayList(this.itemListRecentParty));
        this.itemListPendingParty = this.hHandler.getPendingParty();
        this.LVPendingParty.setItems(FXCollections.observableArrayList(this.itemListPendingParty));
    }

    /**
     * returns the listview
     * @return the listview
     */
    public ListView<HBox> getLVRecentParty() {
        return LVRecentParty;
    }

    /**
     * returns the listview
     * @return
     */
    public ListView<HBox> getLVPendingParty() {
        return LVPendingParty;
    }

    /**
     * method that change the scene to login
     * @param actionEvent
     */
    public void logOut(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Login.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to home
     * @param actionEvent
     */
    public void goHome(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to game
     * @param actionEvent
     */
    public void goGames(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Games.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to social
     * @param actionEvent
     */
    public void goSocial(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Social.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to settings
     * @param actionEvent
     */
    public void goSettings(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Settings.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to userstatistics
     * @param actionEvent
     */
    public void goStatistics(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("UserStatistics.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to administration
     * @param actionEvent
     */
    public void goAdministration(ActionEvent actionEvent) {
        if (this.application.getCurrentUser().getNomrole().equals("admin")){
            this.application.setUrlSceneFXML("Administration.fxml");
            this.application.maj();
        }
        else Alertes.notAnAdministrator();
    }
}