package Gui.Controlers;

import Alertes.Alertes;
import DataHandler.ManagePlayerHandler;
import Gui.Applications.App;
import Users.User;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import java.util.List;

public class ControllerManagePlayer implements Controller{

    public TextField TFSearch;
    public Button BSearch;
    public ListView<HBox> LVPlayers;

    private App application;
    private ManagePlayerHandler mPHandler;
    private List<HBox> formatedUserList;
    private List<User> userList;

    /**
     * method that set the app
     * @param application
     */
    public void setApplication(App application) {
        this.application = application;
        this.mPHandler = new ManagePlayerHandler(this.application.getConnector(), this.application);
        this.userList = mPHandler.getUsers();
        this.formatedUserList = mPHandler.getFormatedUserList(this.userList);
        this.LVPlayers.setItems(FXCollections.observableArrayList(formatedUserList));
    }

    /**
     * method that change the scene to home
     * @param actionEvent
     */
    public void goHome(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to games
     * @param actionEvent
     */
    public void goGames(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Games.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to social
     * @param actionEvent
     */
    public void goSocial(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Social.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to userstatistics
     * @param actionEvent
     */
    public void goStatistics(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("UserStatistics.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to administration
     * @param actionEvent
     */
    public void goAdministration(ActionEvent actionEvent) {
        if (this.application.getCurrentUser().getNomrole().equals("admin")){
            this.application.setUrlSceneFXML("Administration.fxml");
            this.application.maj();
        }
        else Alertes.notAnAdministrator();
    }

    /**
     *  method that cearch players
     * @param actionEvent
     */
    public void search(ActionEvent actionEvent) {
        
    }
}
