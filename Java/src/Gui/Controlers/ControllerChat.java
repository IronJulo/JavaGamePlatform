package Gui.Controlers;

import DataHandler.ChatHandler;
import DataHandler.DataChecker;
import Gui.Applications.App;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.util.List;

public class ControllerChat implements Controller{
    public ImageView IVFriend;
    public Label LNfriend;
    public ListView<HBox> LVChat;
    public TextField TFSend;
    public Button BSend;

    private List<HBox> itemList;
    private ChatHandler cHandler;
    private App application;
    private String fID;

    /**
     * Method that set the app
     * @param app
     */
    @Override
    public void setApplication(App app) {
        this.application = app;
        this.cHandler = new ChatHandler(this.application.getConnector(), this.application);
        this.itemList = this.cHandler.getMessages(this.fID);

        Image img = new Image(DataChecker.getImageLink(this.application.getConnector(), this.application, Integer.parseInt(this.fID)));
        this.IVFriend.setImage(img);
        this.LNfriend.setText(DataChecker.getUsername(this.application.getConnector(), this.application, Integer.parseInt(this.fID)));
        this.LVChat.setItems(FXCollections.observableArrayList(this.itemList));
    }

    /**
     * Update the app
     */
    public void maj(){
        this.itemList = cHandler.getMessages(this.fID);
        this.LVChat.setItems(FXCollections.observableArrayList(this.itemList));
    }

    /**
     * Method that set the friend id
     * @param idFriend
     */
    public void setFriend(String idFriend) {
        this.fID = idFriend;
    }

    /**
     * Method that send the message
     * @param actionEvent
     */
    public void sendMessage(ActionEvent actionEvent) {
        this.cHandler.sendMessage(TFSend.getText(), this.fID);
        this.itemList = cHandler.getMessages(this.fID);
        this.LVChat.setItems(FXCollections.observableArrayList(this.itemList));
        this.TFSend.setText("");
    }
}
