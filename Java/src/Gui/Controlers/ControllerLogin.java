package Gui.Controlers;

import DataHandler.LoginHandler;
import EasterEggs.RickRolled;
import Gui.Applications.App;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;


public class ControllerLogin implements Controller {

    @FXML private TextField TF1;
    @FXML private Button BValidate;
    @FXML private Button CAButton;
    @FXML private TextField TF2;

    private App application;
    private LoginHandler lHandler;

    /**
     * method that set the app
     * @param application
     */
    public void setApplication(App application) {
        this.application = application;
        this.lHandler = new LoginHandler(this.application.getConnector(), this.application);
    }

    /**
     * method that change the scene to register
     * @param actionEvent
     */
    public void CAccount(ActionEvent actionEvent) {
        application.setUrlSceneFXML("Register.fxml");
        this.application.maj();
    }

    /**
     * method that chech and change the scene to home
     * @param actionEvent
     */
    public void VLogin(ActionEvent actionEvent)  {
        if (this.lHandler.checkLogin(TF1.getText(), TF2.getText())){
            application.setUrlSceneFXML("Home.fxml");
            this.lHandler.setUser(this.application, TF1.getText(), this.application.getConnector());
        }
        else if (this.TF1.getText().equals("rick")) RickRolled.rolled();
        this.application.maj();

    }
}