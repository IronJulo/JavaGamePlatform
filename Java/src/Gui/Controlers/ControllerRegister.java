package Gui.Controlers;

import DataHandler.RegisterHandler;
import Gui.Applications.App;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ControllerRegister implements Controller {
    public TextField TF1;
    public TextField TF2;
    public TextField TF3;
    public Button BCAccount;
    public Button BLogin;
    public Button BValidate;

    private App application;
    private RegisterHandler rHandler;

    /**
     * method that set the app
     * @param app
     */
    @Override
    public void setApplication(App app) {
        this.application = app;
        this.rHandler = new RegisterHandler(this.application.getConnector(), this.application);
    }

    /**
     * method that create a new account
     * @param actionEvent
     */
    public void CAccount(ActionEvent actionEvent) {
        rHandler.createUser(this.TF1.getText(), this.TF2.getText(), this.TF3.getText());
    }

    /**
     * method that change the scene to login
     * @param actionEvent
     */
    public void VLogin(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Login.fxml");
        this.application.maj();
    }
}
