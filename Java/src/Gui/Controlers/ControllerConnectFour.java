package Gui.Controlers;

import Alertes.Alertes;
import DataHandler.ConnectFourlHandler;
import DataHandler.DataChecker;
import Gui.Applications.App;
import Gui.Controlers.Game.Game;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class ControllerConnectFour implements Controller {
    private App application;
    private int idP;
    public static Timeline timeline;
    private static final int STARTTIME = 0;
    private final IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);

    /**
     * method that set the app
     * @param app
     */
    @Override
    public void setApplication(App app) {
        this.application = app;
    }

    /**
     * method that set the party id
     * @param idPartie
     */
    public void setId(int idPartie){
        this.idP = idPartie;

        VBox y = (VBox) this.application.getStage().getScene().lookup("#Yellow");
        VBox r = (VBox) this.application.getStage().getScene().lookup("#Red");
        ConnectFourlHandler lHandler = new ConnectFourlHandler(this.application.getConnector(), this.application);

        ((ImageView) y.getChildren().get(0)).setImage(new Image(DataChecker.getImageLink(this.application.getConnector(),this.application,Integer.parseInt(lHandler.getPartieGuessId(this.idP)))));
        ((Text) y.getChildren().get(1)).setText(DataChecker.getUsername(this.application.getConnector(),this.application,Integer.parseInt(lHandler.getPartieGuessId(this.idP))));
        if (((Text) y.getChildren().get(1)).getText().equals(application.getCurrentUser().getName())) ((Text) y.getChildren().get(1)).setText(((Text) y.getChildren().get(1)).getText()+" (You)");

        ((ImageView) r.getChildren().get(0)).setImage(new Image(DataChecker.getImageLink(this.application.getConnector(),this.application,Integer.parseInt(lHandler.getPartieOwnerId(this.idP)))));
        ((Text) r.getChildren().get(1)).setText(DataChecker.getUsername(this.application.getConnector(),this.application,Integer.parseInt(lHandler.getPartieOwnerId(this.idP))));
        if (((Text) r.getChildren().get(1)).getText().equals(application.getCurrentUser().getName())) ((Text) r.getChildren().get(1)).setText(((Text) r.getChildren().get(1)).getText()+" (You)");
        Game.reload(this.application,(HBox) this.application.getStage().getScene().lookup("#Lvl"), this.idP);

        timeline = new Timeline(new KeyFrame(Duration.seconds(2), evt -> updateTime()));
        timeline.setCycleCount(Animation.INDEFINITE); // repeat over and over again
        timeSeconds.set(STARTTIME);
        timeline.play();
    }

    /**
     * Update the time
     */
    private void updateTime() {
        // increment seconds
        int seconds = timeSeconds.get();
        timeSeconds.set(seconds+1);
        Game.reload(this.application,(HBox) this.application.getStage().getScene().lookup("#Lvl"), this.idP);
    }

    /**
     * method that change the scene to home
     * @param actionEvent
     */
    public void goHome(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to games
     * @param actionEvent
     */
    public void goGames(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Games.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to social
     * @param actionEvent
     */
    public void goSocial(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Social.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to settings
     * @param actionEvent
     */
    public void goSettings(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Settings.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to userstatistics
     * @param actionEvent
     */
    public void goStatistics(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("UserStatistics.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to administration
     * @param actionEvent
     */
    public void goAdministration(ActionEvent actionEvent) {
        if (this.application.getCurrentUser().getNomrole().equals("admin")){
            this.application.setUrlSceneFXML("Administration.fxml");
            this.application.maj();
        }
        else Alertes.notAnAdministrator();
    }

    /**
     * method that relaod the game
     * @param actionEvent
     */
    public void goReload(ActionEvent actionEvent) {
        Game.reload(this.application, (HBox) this.application.getStage().getScene().lookup("#Lvl"), this.idP);
    }

    /**
     * method used to surrender
     * @param actionEvent
     */
    public void goSurrender(ActionEvent actionEvent) {
        int idGagnant;
        ConnectFourlHandler lHandler = new ConnectFourlHandler(this.application.getConnector(), this.application);

        if(application.getCurrentUser().getName().equals(  DataChecker.getUsername(this.application.getConnector(),this.application, Integer.parseInt(lHandler.getPartieGuessId( this.idP)) ))) idGagnant=Integer.parseInt(lHandler.getPartieOwnerId( this.idP));
        else idGagnant=Integer.parseInt(lHandler.getPartieGuessId( this.idP));
        ConnectFourlHandler.partyOver(this.idP, idGagnant, this.application.getConnector());
        timeline.stop();
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method used to leave
     * @param actionEvent
     */
    public void goLeave(ActionEvent actionEvent) {
        timeline.stop();
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method that insert pawns
     * @param mouseEvent
     */
    public void ajouterPion(MouseEvent mouseEvent) {
        HBox lvl = (HBox) this.application.getStage().getScene().lookup("#Lvl");

        switch(((VBox) mouseEvent.getTarget()).getId()) {
            case "C1":
                Game.ajouterPion(0, lvl, this.application, this.idP);
                break;
            case "C2":
                Game.ajouterPion(1, lvl, this.application, this.idP);
                break;
            case "C3":
                Game.ajouterPion(2, lvl, this.application, this.idP);
                break;
            case "C4":
                Game.ajouterPion(3, lvl, this.application, this.idP);
                break;
            case "C5":
                Game.ajouterPion(4, lvl, this.application, this.idP);
                break;
            case "C6":
                Game.ajouterPion(5, lvl, this.application, this.idP);
                break;
            case "C7":
                Game.ajouterPion(6, lvl, this.application, this.idP);
                break;
        }
    }
}