package Gui.Controlers;

import Alertes.Alertes;
import DataHandler.DataChecker;
import DataHandler.SocialHandler;
import DataHandler.Statistics.SocialStatisticsHandler;
import Gui.Applications.App;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import java.util.List;

import org.controlsfx.control.textfield.TextFields;

public class ControllerSocial implements Controller {
    public Label LSent;
    public Label LRecieved;
    public Label LTotal;
    public Label LUnreadMessage;
    public TextField TFFriendName;
    public Label LTitle;
    public ListView<HBox> LVFriends;

    private App application;
    private SocialHandler sHandler;
    private List<HBox> itemList;

    /**
     * method that set the app
     * @param application
     */
    public void setApplication(App application) {
        this.application = application;
        this.sHandler = new SocialHandler(this.application.getConnector(), application);
        this.LRecieved.setText("You have recieved " + SocialStatisticsHandler.getNReceivedInvitation(this.application.getConnector(), this.application) + " friends requests");
        this.LSent.setText("You have sended " + SocialStatisticsHandler.getNSentInvitation(this.application.getConnector(), this.application) + " friends requests");
        this.LTotal.setText("You have " + SocialStatisticsHandler.getNFriends(this.application.getConnector(), this.application) + " friends");
        this.LUnreadMessage.setText("You have " + SocialStatisticsHandler.getNUnreadMessages(this.application.getConnector(), this.application) + " unread messages");
        this.itemList = sHandler.getFriendList();
        this.LVFriends.setItems(FXCollections.observableArrayList(this.itemList));
        TextFields.bindAutoCompletion(this.TFFriendName, DataChecker.getAllUser(this.application.getConnector(), this.application));
    }

    /**
     * method that change the scene to home
     * @param actionEvent
     */
    public void goHome(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to games
     * @param actionEvent
     */
    public void goGames(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Games.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to social
     * @param actionEvent
     */
    public void goSocial(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Social.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to userstatistics
     * @param actionEvent
     */
    public void goStatistics(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("UserStatistics.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to administration
     * @param actionEvent
     */
    public void goAdministration(ActionEvent actionEvent) {
        if (this.application.getCurrentUser().getNomrole().equals("admin")){
            this.application.setUrlSceneFXML("Administration.fxml");
            this.application.maj();
        }
        else Alertes.notAnAdministrator();
    }

    /**
     * method that change the scene tosend an invitation
     * @param actionEvent
     */
    public void sendRequest(ActionEvent actionEvent) {
        this.sHandler.sendInvitation(this.application.getConnector(), this.application, this.TFFriendName.getText());
        this.application.maj();
    }

    /**
     * method that display all sent request
     * @param actionEvent
     */
    public void displaySentRequest(ActionEvent actionEvent) {
        this.itemList = sHandler.getFriendRequestList();
        this.LVFriends.setItems(FXCollections.observableArrayList(this.itemList));
        this.LTitle.setText("Friend request sent");
        
    }

    /**
     * method that display all received request
     * @param actionEvent
     */
    public void displayReceivedRequest(ActionEvent actionEvent) {
        this.itemList = sHandler.getFriendReceivedRequestList();
        this.LVFriends.setItems(FXCollections.observableArrayList(this.itemList));
        this.LTitle.setText("Friend request received");
    }

    /**
     * Method that display all friends
     * @param actionEvent
     */
    public void displayFriends(ActionEvent actionEvent) {
        this.itemList = sHandler.getFriendList();
        this.LVFriends.setItems(FXCollections.observableArrayList(this.itemList));
        this.LTitle.setText("Friend Lists");
    }
}