package Gui.Controlers;

import DataHandler.SocialHandler;
import Gui.Applications.App;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControllerFriendRequestSend implements EventHandler<ActionEvent> {
    private App application;
    private SocialHandler sHandler;
    private String fID;

    /**
     *
     * @param application
     * @param sc
     * @param friendId
     */
    public ControllerFriendRequestSend(App application, SocialHandler sc, String friendId){
        this.application = application;
        this.sHandler = sc;
        this.fID = friendId;
    }

    /**
     * method that handle the buttons
     * @param event
     */
    @Override
    public void handle(ActionEvent event) {
        if (((Button) event.getTarget()).getText().equals("Cancel Friend request")){
            this.sHandler.cancelFriendRequest(this.application.getConnector(), this.application, this.fID);
            this.application.maj();
        }    
    }
}