package Gui.Controlers;

import DataHandler.ConnectFourlHandler;
import Gui.Applications.App;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControllerJoinGiveUpParty implements EventHandler<ActionEvent>{
    private App application;
    private int idP;
    private int j1;
    private int j2;

    /**
     *
     * @param application
     * @param idPartie
     * @param j1
     * @param j2
     */
    public ControllerJoinGiveUpParty(App application, int idPartie, int j1, int j2){
        this.application = application;
        this.idP = idPartie;
        this.j1 = j1;
        this.j2 = j2;
    }

    /**
     * method that handle the buttons
     * @param event
     */
    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) event.getTarget();
        if (button.getText().equals("Join the game")){
            this.application.setUrlSceneFXML("ConnectFourGame.fxml");
            this.application.maj();
            ((ControllerConnectFour) this.application.getController()).setId(this.idP);   
        }
        if (button.getText().equals("Give up")){
            int idGagnant;
            if(this.application.getCurrentUser().getId()==this.j1) idGagnant = this.j2;
            else idGagnant = this.j1;
            ConnectFourlHandler.partyOver(this.idP, idGagnant, this.application.getConnector());
            this.application.maj();
        }
    }
}