package Gui.Controlers;


import Alertes.Alertes;
import DataHandler.SettingsHandler;
import Gui.Applications.App;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ControllerSettings implements Controller {
    public Button BHome;
    public Button BGames;
    public Button BSocial;
    public Button BStatistics;
    public Button BAdministration;
    public ImageView IVProfile;
    public Label LUsername;
    public Label LMail;
    public Button BDeleteAccount;
    public TextField TFUsername;
    public TextField TFMail;
    public TextField TFPassword;
    public TextField TFConfirmPassword;
    public Button CUsername;
    public Button CMail;
    public Button CCPassword;
    public Label LInformation;
    public TextField TFProfilPicture;
    public Button CProfilPicture;

    private App application;
    private SettingsHandler sdHandler;

    /**
     * method that set the app
     * @param application
     */
    public void setApplication(App application) {
        this.application = application;
        this.LUsername.setText(this.application.getCurrentUser().getName());
        this.LMail.setText(this.application.getCurrentUser().getMail());
        this.IVProfile.setImage(new Image(this.application.getCurrentUser().getPpLink()));
        this.sdHandler = new SettingsHandler(this.application.getConnector(), this.application);
        if (this.application.getCurrentUser().getNomrole().equals("admin")){
            this.LInformation.setText("You are an administrator !");
        }
        else this.LInformation.setText("You are not an administrator !");
    }

    /**
     * method that change the scene to home
     * @param actionEvent
     */
    public void goHome(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to games
     * @param actionEvent
     */
    public void goGames(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Games.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to social
     * @param actionEvent
     */
    public void goSocial(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Social.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to login
     * @param actionEvent
     */
    public void logOut(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Login.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to settings
     * @param actionEvent
     */
    public void goSettings(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Settings.fxml");
        this.application.maj();

    }

    /**
     * method that change the scene to userstatistics
     * @param actionEvent
     */
    public void goStatistics(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("UserStatistics.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to administration
     * @param actionEvent
     */
    public void goAdministration(ActionEvent actionEvent) {
        if (this.application.getCurrentUser().getNomrole().equals("admin")){
            this.application.setUrlSceneFXML("Administration.fxml");
            this.application.maj();
        }
        else Alertes.notAnAdministrator();
    }

    /**
     * method that delete the account
     * @param actionEvent
     */
    public void deleteAccount(ActionEvent actionEvent) {
        if (Alertes.confirmDeleteAccount()){
            this.sdHandler.deleteAccount();
            this.application.setUrlSceneFXML("Login.fxml");
            this.application.maj();
        }
    }

    /**
     * method that change the user username
     * @param actionEvent
     */
    public void confirmNewUsername(ActionEvent actionEvent) {
        this.sdHandler.changeUsername(TFUsername.getText());
        this.application.maj();
    }

    /**
     * method that change the user mail
     * @param actionEvent
     */
    public void ConfirmNewMail(ActionEvent actionEvent) {
        this.sdHandler.changeMail(TFMail.getText());
        this.application.maj();
    }

    /**
     * method that change the user password
     * @param actionEvent
     */
    public void ConfirmNewPassword(ActionEvent actionEvent) {
        this.sdHandler.changePassword(TFPassword.getText(), TFConfirmPassword.getText());
        this.application.maj();
    }

    /**
     * method that change the user profile picture
     * @param actionEvent
     */
    public void CnewProfilPicture(ActionEvent actionEvent) {
            if (this.TFProfilPicture.getText().equals("")) {
                this.sdHandler.changeProfilPicture("https://static.thenounproject.com/png/17241-200.png");
            }
            else if(this.TFProfilPicture.getText().length()>500){
                Alertes.invalidImageLink();
            } 
            else {
                this.sdHandler.changeProfilPicture(this.TFProfilPicture.getText());
            }
    
        this.application.maj();
    }
}
