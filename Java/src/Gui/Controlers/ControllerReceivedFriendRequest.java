package Gui.Controlers;

import DataHandler.SocialHandler;
import Gui.Applications.App;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControllerReceivedFriendRequest implements EventHandler<ActionEvent> {
    private App application;
    private SocialHandler sHandler;
    private String fID;

    /**
     *
     * @param application
     * @param sc
     * @param friendId
     */
    public ControllerReceivedFriendRequest(App application, SocialHandler sc, String friendId){
        this.application = application;
        this.sHandler = sc;
        this.fID = friendId;
    }

    /**
     * method that handle the buttons
     * @param event
     */
    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) event.getTarget();
        if (button.getText().equals("Accept Friend Request")){
            this.sHandler.acceptFriendRequest(this.application.getConnector(), this.application, this.fID);
        }
        if (button.getText().equals("Delete Friend Request")){
            this.sHandler.deleteFriendRequest(this.application.getConnector(), this.application, this.fID);
        }
        this.application.maj();
    }
}