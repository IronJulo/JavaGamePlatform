package Gui.Controlers.Game;

import java.util.Arrays;

import DataHandler.ConnectFourlHandler;
import DataHandler.DataChecker;
import Gui.Applications.App;
import Gui.Controlers.ControllerConnectFour;
import javafx.scene.control.Alert;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

public class Game {
    /**
     *
     * @param app
     * @param lvl
     * @param idPartie
     */
    public static void reload(App app, HBox lvl, int idPartie){
        afficherLaGrid(GameJson.getGrid(app, idPartie), lvl, app, idPartie);
    }

    /**
     * Method that dispmlay the grid
     * @param grid
     * @param lvl
     * @param app
     * @param idPartie
     */
    public static void afficherLaGrid(Integer[][] grid, HBox lvl, App app, int idPartie){
        for(int i=0; i<grid.length; ++i){
            for(int j=0; j<grid[i].length; ++j){
                if (grid[i][j]==1) ((Circle) ((VBox) lvl.getChildren().get(i)).getChildren().get(j)).setFill(Paint.valueOf("RED"));

                else if (grid[i][j]==2) ((Circle) ((VBox) lvl.getChildren().get(i)).getChildren().get(j)).setFill(Paint.valueOf("YELLOW"));

                else ((Circle) ((VBox) lvl.getChildren().get(i)).getChildren().get(j)).setFill(Paint.valueOf("#d8f1fc"));
            }
        }

        VBox y = (VBox) app.getStage().getScene().lookup("#Yellow");
        VBox r = (VBox) app.getStage().getScene().lookup("#Red");

        if (GameJson.canPlay(app, idPartie, Integer.parseInt((new ConnectFourlHandler(app.getConnector(), app)).getPartieGuessId(idPartie)))){
            ((Text) y.getChildren().get(2)).setText(" (Playing)");
            ((Text) r.getChildren().get(2)).setText(" (Wait)");
        }else{
            ((Text) y.getChildren().get(2)).setText(" (Wait)");
            ((Text) r.getChildren().get(2)).setText(" (Playing)");
        }

        Integer over = isOver(grid);
        if(over!=0){
            ControllerConnectFour.timeline.stop();
            ConnectFourlHandler lHandler = new ConnectFourlHandler(app.getConnector(), app);
            Integer gagnaId;
            if(over == 1) gagnaId = Integer.parseInt(lHandler.getPartieOwnerId(idPartie));
            else gagnaId = Integer.parseInt(lHandler.getPartieGuessId(idPartie));
            lHandler.partyOver(idPartie, gagnaId);

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Finished game");
            alert.setHeaderText(DataChecker.getUsername(app.getConnector(),app, gagnaId)+" is the Winner");
            alert.setContentText("What a beautiful match ! ;-)");
            alert.showAndWait();
            app.setUrlSceneFXML("Home.fxml");
            app.maj();
        }
    }

    /**
     * Method that add the pawn
     * @param id
     * @param lvl
     * @param app
     * @param idPartie
     */
    public static void ajouterPion(Integer id, HBox lvl, App app, int idPartie){
        Integer[][] grid = GameJson.getGrid(app, idPartie);
        if (GameJson.canPlay(app, idPartie)){
            Boolean placementReussi = false;
            for(int i= grid[id].length-1; i>=0; i--){
                if (grid[id][i] ==0){
                    placementReussi = true;
                    if(String.valueOf(app.getCurrentUser().getId()).equals((new ConnectFourlHandler(app.getConnector(), app)).getPartieOwnerId(idPartie))) grid[id][i] = 1;
                    else grid[id][i] = 2;
                    break;
                }
            }
            if(placementReussi) GameJson.saveGrid(app, grid, idPartie);
        }
        else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("You can't make that move");
            alert.setHeaderText("Please wait until it's your turn to play");
            alert.showAndWait();
        }
        afficherLaGrid(grid, lvl, app, idPartie);
    }

    /**
     * returns null if not finished, true if there is a winner, false if there isn't
     * @param grid
     * @return null if not finished, true if there is a winner, false if there isn't
     */
    public static Integer isOver(Integer[][] grid){
        for(int i=0; i<grid.length; i++){
            for(int j=grid[i].length-1; j>=0; j--){
                if(j>2){ // VERTICAL
                    if(grid[i][j]==1 && grid[i][j-1]==1 && grid[i][j-2]==1 && grid[i][j-3]==1) return 1;
                    else if(grid[i][j]==2 && grid[i][j-1]==2 && grid[i][j-2]==2 && grid[i][j-3]==2) return 2;
                }
                if(i>2){ // HORIZONTAL
                    if(grid[i][j]==1 && grid[i-1][j]==1 && grid[i-2][j]==1 && grid[i-3][j]==1) return 1;
                    else if(grid[i][j]==2 && grid[i-1][j]==2 && grid[i-2][j]==2 && grid[i-3][j]==2) return 2;
                }
                if(j>2 && i<grid.length-3){ // RIGHT DIAGONAL
                    if(grid[i][j]==1 && grid[i+1][j-1]==1 && grid[i+2][j-2]==1 && grid[i+3][j-3]==1) return 1;
                    else if(grid[i][j]==2 && grid[i+1][j-1]==2 && grid[i+2][j-2]==2 && grid[i+3][j-3]==2) return 2;
                }
                if(j>2 && i>2){ // LEFT DIAGONAL
                    if(grid[i][j]==1 && grid[i-1][j-1]==1 && grid[i-2][j-2]==1 && grid[i-3][j-3]==1) return 1;
                    else if(grid[i][j]==2 && grid[i-1][j-1]==2 && grid[i-2][j-2]==2 && grid[i-3][j-3]==2) return 2;
                }
            }
        }
        return 0;
    }

    /**
     * returns the grid
     * @return the grid
     */
    public static Integer[][] recupererLeNiveau(){
        Integer[][] grid = new Integer[7][6];
        for(Integer[] g: grid) Arrays.fill(g,0);
        return grid;
    }
}