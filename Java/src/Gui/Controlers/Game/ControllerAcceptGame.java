package Gui.Controlers.Game;

import DataHandler.DataChecker;
import Gui.Applications.App;
import Gui.Controlers.ControllerConnectFour;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControllerAcceptGame implements EventHandler<ActionEvent>{
    private App application;
    private int id_expediteur;
    private String gameName;
    public ControllerAcceptGame(App application, int id_expediteur, String gameName){
        this.application = application;
        this.id_expediteur = id_expediteur;
        this.gameName = gameName;
    }

    /**
     * Method that handle the invitation request
     * @param event
     */
    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) event.getTarget();
        if (button.getText().equals("Accept Game Invitation")){
            DataChecker.createParty(this.application.getConnector(), this.application, this.id_expediteur);
            DataChecker.deleteInvitationParty(this.application.getConnector(), this.application, this.id_expediteur, this.gameName);
            this.application.setUrlSceneFXML("ConnectFourGame.fxml");
            this.application.maj();
            ((ControllerConnectFour) this.application.getController()).setId(DataChecker.getLastParty(this.application.getConnector(), this.application));    
        }
        if (button.getText().equals("Delete Game Invitation")){
            DataChecker.deleteInvitationParty(this.application.getConnector(), this.application, this.id_expediteur, this.gameName);
        }
    }
    
}