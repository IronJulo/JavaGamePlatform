package Gui.Controlers.Game;

import DataHandler.ConnectFourlHandler;
import Gui.Applications.App;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.StringWriter;


public class GameJson {

    /**
     * returns the json object
     * @param s
     * @return the json object
     *
     */
    public static JSONObject stringToJson(String s){
        JSONObject res = new JSONObject();
        try {
            res = (JSONObject) ((new JSONParser()).parse(  s ));
        }catch (ParseException pe){
            System.out.println("position: " + pe.getPosition());
            System.out.println(pe);
        }
        return res;
    }

    /**
     * turns the table into string
     * @param grid
     * @return the string
     */
    public static String gridtoString(Integer[][] grid){
        String strGrid = "";
        for(int i=0; i<grid.length; ++i){
            for(int j=0; j<grid[i].length; ++j){
                strGrid += (String.valueOf(grid[i][j]));
            }
        }
        return strGrid;
    }

    /**
     * turns the string into a table
     * @param strGrid
     * @return the table
     */
    public static Integer[][] stringtoGrid(String strGrid){
        Integer[][] grid = new Integer[7][6];
        for(int i=0; i<grid.length; ++i){
            for(int j=0; j<grid[i].length; ++j){
                grid[i][j]=Character.getNumericValue(strGrid.charAt(i*6+j));
            }
        }
        return grid;
    }

    /**
     * Method taht get the json from the db
     * @param app
     * @param idPartie
     * @return the json object
     */
    public static JSONObject getJson(App app, int idPartie){
        return stringToJson((new ConnectFourlHandler(app.getConnector(), app)).getJsonFile(idPartie));
    }

    /**
     * returns true if able to play
     * @param app
     * @param idPartie
     * @return true if able to play
     */
    public static Boolean canPlay(App app, int idPartie){
        return ! String.valueOf(app.getCurrentUser().getId()).equals(Long.toString((Long) (getJson(app, idPartie).get("last"))));
    }

    /**
     * returns true if able to play
     * @param app
     * @param idPartie
     * @param idPlayer
     * @return true if able to play
     */
    public static Boolean canPlay(App app, int idPartie, int idPlayer){
        return ! String.valueOf(idPlayer).equals(Long.toString((Long) (getJson(app, idPartie).get("last"))));
    }

    /**
     * returns the table
     * @param app
     * @param idPartie
     * @return the table
     */
    public static Integer[][] getGrid(App app, int idPartie){
        return stringtoGrid(String.valueOf(getJson(app, idPartie).get("grid")));
    }

    /**
     * Method that save the json
     * @param app
     * @param obj
     * @param idPartie
     */
    public static void saveJson(App app, JSONObject obj, int idPartie){
        ConnectFourlHandler lHandler = new ConnectFourlHandler(app.getConnector(), app);

        StringWriter out = new StringWriter();
        try {
            obj.writeJSONString(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        lHandler.reWriteJsonFile(idPartie, out.toString());
    }

    /**
     * Method that save the grid
     * @param app
     * @param grid
     * @param idPartie
     */
    public static void saveGrid(App app, Integer[][] grid, int idPartie){
        JSONObject obj = new JSONObject();
        
        obj.put("grid", gridtoString(grid));
        obj.put("last", app.getCurrentUser().getId());  //Le dernier jouer à avoir joué.

        saveJson(app, obj, idPartie);
    }
}