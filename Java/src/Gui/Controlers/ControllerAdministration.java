package Gui.Controlers;

import Alertes.Alertes;
import DataHandler.AdministrationHandler;
import Gui.Applications.App;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

public class ControllerAdministration implements Controller {
    public Button BHome, BGames, BSocial, BStatistics, BAdministration, BRevokeRights;
    public HBox BManagePlayers, BPlatfromStatistics;

    private App application;
    private AdministrationHandler aHandler;

    /**
     * Method taht set the app
     * @param application
     */
    public void setApplication(App application) {
        this.application = application;
        this.aHandler = new AdministrationHandler(this.application.getConnector(), this.application);
    }

    /**
     * method that change the scene to home
     * @param actionEvent
     */
    public void goHome(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Home.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to games
     * @param actionEvent
     */
    public void goGames(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Games.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to social
     * @param actionEvent
     */
    public void goSocial(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("Social.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to userstatistics
     * @param actionEvent
     */
    public void goStatistics(ActionEvent actionEvent) {
        this.application.setUrlSceneFXML("UserStatistics.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to the admin page
     * @param actionEvent
     */
    public void goAdministration(ActionEvent actionEvent) {
        if (this.application.getCurrentUser().getNomrole().equals("admin")) Alertes.notAnAdministrator();
        else{
            this.application.setUrlSceneFXML("Administration.fxml");
            this.application.maj();
        }
    }

    /**
     * Method tahr revoke admin rights
     * @param actionEvent
     */
    public void revokeRights(ActionEvent actionEvent) {
        if (Alertes.confirmRights("Revoke Rights", "Are you sure about that ?", null)){
            aHandler.revokeRight();
            this.application.setUrlSceneFXML("Login.fxml");
            this.application.maj();
        }
    }

    /**
     * method that change the scene to manageplayers
     * @param mouseEvent
     */
    public void goManagePlayer(MouseEvent mouseEvent) {
        this.application.setUrlSceneFXML("ManagePlayers.fxml");
        this.application.maj();
    }

    /**
     * method that change the scene to platformstatistics
     * @param mouseEvent
     */
    public void goPlatformStatistics(MouseEvent mouseEvent) {
        this.application.setUrlSceneFXML("PlatformStatistics.fxml");
        this.application.maj();
    }
}
