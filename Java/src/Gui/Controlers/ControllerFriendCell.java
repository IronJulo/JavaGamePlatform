package Gui.Controlers;

import Gui.Applications.App;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import DataHandler.SocialHandler;

public class ControllerFriendCell implements EventHandler<ActionEvent>{
    private App application;
    private Parent root;
    private ControllerChat controller;
    private String fID;
    private SocialHandler sHandler;
    public static Timeline timeline;
    private static final int STARTTIME = 0;
    private final IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);

    /**
     * method that set the app
     * @param application
     */
    public void setApplication(App application) {
        this.application = application;
    }

    /**
     * method that set the friend
     * @param idFriend
     * @param sc
     */
    public void setFriend(String idFriend, SocialHandler sc) {
        this.fID = idFriend;
        this.sHandler = sc;
    }

    /**
     * method that handle the buttons
     * @param event
     */
    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) event.getTarget();
        if (button.getText().equals("Chat")){
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../Ressources/Scenes/Chat.fxml"));
            try {
                this.root = (Parent) loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage newWindow = new Stage();
            newWindow.setTitle("Chat");
            newWindow.setScene(new Scene(root));
            newWindow.setOnCloseRequest(new CloseChat());
            this.controller = loader.getController();
            this.controller.setFriend(this.fID);
            this.controller.setApplication(this.application);
            newWindow.show();
            timeline = new Timeline(new KeyFrame(Duration.seconds(3), evt -> updateTime()));
            timeline.setCycleCount(Animation.INDEFINITE); // repeat over and over again
            timeSeconds.set(STARTTIME);
            timeline.play();
        }
        else if (button.getText().equals("Delete Friend")) {
            this.sHandler.deleteFriend(this.application.getConnector(), this.application, this.fID);
            this.application.maj();
        }
    }

    /**
     * method that update the time
     */
    private void updateTime() {
        int seconds = timeSeconds.get();
        timeSeconds.set(seconds+1);
        this.controller.maj();
    }
}
