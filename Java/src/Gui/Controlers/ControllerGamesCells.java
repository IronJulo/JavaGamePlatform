package Gui.Controlers;

import DataHandler.DataChecker;
import Games.Games;
import Gui.Applications.App;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.List;

import Alertes.Alertes;

public class ControllerGamesCells implements Controller{
    public Button BMatchMaking;
    public Button BInviteFriend;
    public Label LGameName;
    public Label LGameDescription;
    public ImageView IVGame;
    public ComboBox<String> ComboBoxFriends;
    private App application;
    private Games game;
    private List<String> friends;

    /**
     * method that set the app
     * @param app
     */
    @Override
    public void setApplication(App app) {
        this.application = app;
        this.LGameDescription.setText(this.game.getGameDescription());
        this.LGameName.setText(this.game.getGameName());
        Image image = new Image(this.game.getGamePictureLink());
        this.IVGame.setImage(image);
        this.IVGame.setPreserveRatio(true);
        this.IVGame.setFitHeight(100);
        this.friends = DataChecker.getFriendList(this.application.getConnector(), this.application);
        this.ComboBoxFriends.getItems().addAll(this.friends);
    }

    /**
     *
     * @param game
     */
    public void setGame(Games game){
        this.game = game;
    }

    /**
     * method that is used to paly match making
     * @param actionEvent
     */
    public void playMatchMaking(ActionEvent actionEvent) {
        if(DataChecker.someoneWait(this.application.getConnector(), this.application)){
            DataChecker.createParty(this.application.getConnector(), this.application, DataChecker.getWaiterId(this.application.getConnector(), this.application));
            DataChecker.deleteWaiter(this.application.getConnector(), this.application, DataChecker.getWaiterId(this.application.getConnector(), this.application));
            this.application.setUrlSceneFXML("ConnectFourGame.fxml");
            this.application.maj();
            ((ControllerConnectFour) this.application.getController()).setId(DataChecker.getLastParty(this.application.getConnector(), this.application));
        }
        else{
            DataChecker.waitUser(this.application.getConnector(), this.application);
            Alertes.userHaveToWait(); 
        }
    }

    /**
     *
     * @param actionEvent
     */
    public void inviteFriend(ActionEvent actionEvent) {
        @SuppressWarnings("unchecked")
        String friendName = ((ComboBox<String>) actionEvent.getTarget()).getValue();
        DataHandler.GamesHandler.sendInvitationGame(this.application.getConnector(), this.application, friendName, this.game.getGameName());
    }
}