package Gui.Applications;

import DataHandler.JdbcConnector;
import Gui.Controlers.Controller;

import Users.User;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
    private String urlSceneFXML;
    private FXMLLoader loader;
    private Stage stage;
    private Parent root;
    private Controller actualControler;
    private JdbcConnector connector;
    private User currentUser;
    private boolean centerScreen;

    /**
     * Method that set the url
     * @param urlSceneFXML
     */
    public void setUrlSceneFXML(String urlSceneFXML) {
        this.urlSceneFXML = "../../Ressources/Scenes/" + urlSceneFXML;
    }

    /**
     * returns the controller
     * @return the controller
     */
     public Controller getController(){
        return this.actualControler;
     }

    /**
     * Method that set the user
     * @param user
     */
    public void setCurrentUser(User user){this.currentUser = user;}

    /**
     * returns the user
     * @return the user
     */
    public User getCurrentUser() {return this.currentUser;}

    /**
     * returns the fxml link
     * @return the fxml link
     */
    public String getUrlSceneFXML() {return this.urlSceneFXML;}

    /**
     * returns the connector
     * @return the connector
     */
    public JdbcConnector getConnector() {return this.connector;}

    /**
     * returns the stage
     * @return the stage
     */
    public Stage getStage() {return this.stage;}

    /**
     * Method that setup the app
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        this.setUrlSceneFXML("Login.fxml");
        this.loader = new FXMLLoader(getClass().getResource(this.urlSceneFXML));
        this.root = (Parent) loader.load();
        this.stage = stage;
        this.actualControler = loader.getController();
        this.connector = new JdbcConnector();
        this.actualControler.setApplication(this);
        this.centerScreen = false;

        stage.setScene(new Scene(root));

        stage.show();
    }

    /**
     * update the app
     */
    public void maj(){
        this.loader = new FXMLLoader(getClass().getResource(this.urlSceneFXML));
        try {
            this.root = (Parent) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.actualControler = loader.getController();
        this.actualControler.setApplication(this);
        this.stage.setScene(new Scene(root));
        this.stage.setWidth(this.stage.getWidth());
        this.stage.setHeight(this.stage.getHeight());
        this.stage.setX(this.stage.getX());
        this.stage.setY(this.stage.getY());
        if (!this.centerScreen) {
            this.stage.centerOnScreen();
            this.centerScreen = true;
        }
    }

    /**
     * the main methode
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}

/*

                |-----------|
                |   HELLO   |
                |-----------|
                (\__/) ||
                (•ㅅ•) ||
                / 　 \


 */